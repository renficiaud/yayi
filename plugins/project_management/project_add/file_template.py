﻿#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud

# exemple : 
# python file_template.py "Reconstruction" "REC"


import os, sys, re, string, types
print ' -- Loading %s (%s)' % (__name__, __file__)

parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)
sys.path.append(os.path.join(parent_dir, u""))



def transform_keywords(file_input, file_output, dict_keywords):
  """Transforms a template file with some commands to a usable file file"""
  current_file_in = file(file_input, 'r')
  current_file_out= file(file_output, 'w')
  for line in current_file_in:
    s = string.Template(line).safe_substitute(dict_keywords)
    #print line, '--->', s
    current_file_out.write(s)


  current_file_in.close()
  current_file_out.close()





  return 

def universal_path_join(root, element):
  if(type(element) is types.TupleType):
    out = root
    for i in element: out = os.path.join(out, i)
    return out
  return os.path.join(root, element)


root = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir) #'' # points on Yayi
root_on_file_sys = 'temp'#os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir))
def create_directories_and_files(project_name):

  project_root = 'yayi' + project_name
  project_root_tests = project_root + 'Tests'
  project_root_python= project_root + 'Python'

  return [  
            # core files
            [(root, 'core', project_root), ('CMakeListsTemplate.txt', 'CMakeLists.txt') ],
            [(root, 'core', project_root), (('core', 'main.hpp'), 'yayi' + project_name + '.hpp') ],
            [(root, 'core', project_root, 'src') ],
            
            # test files
            [(root, 'coreTests', project_root_tests), (('test', 'main.hpp'), 'main.hpp') ],
            [(root, 'coreTests', project_root_tests), (('test', 'main.cpp'), 'main.cpp') ],

            
            # python files
            [(root, 'python', project_root_python), (('python', 'main_python.hpp'), project_name.lower() + '_python.hpp') ],
            [(root, 'python', project_root_python), (('python', 'main_python.cpp'), project_name.lower() + '_python.cpp') ],
            [(root, 'python', project_root_python), (('python', 'test_python.py'), project_name.lower() + '_python.py') ],
            
            
          ]


def create_structure(project_name, dict):
  """Creates the arborescent structure of directories and files"""
  
  d = create_directories_and_files(project_name)
  
  for entry in d:
    #print entry
    #print len(entry)
    dir_dest = universal_path_join(root_on_file_sys, entry[0])

    if(not os.path.exists(dir_dest)):
      print 'Creating directory', os.path.abspath(dir_dest)
      os.makedirs(dir_dest)
    
    if(len(entry) > 1):
      file_to_copy = universal_path_join(os.path.dirname(__file__), entry[1][0])
      file_output  = os.path.join(dir_dest, entry[1][1])
      print 'Constructing file', file_output
      transform_keywords(file_to_copy, file_output, dict)
    
  

  
if __name__ == '__main__':
  if(len(sys.argv) < 3):
    print "Bad number of arguments"
    print "first: project name"
    print "second: project export name"
    print "third: project function export flag (for DLL)"
    sys.exit(-1)
  
  project_name = sys.argv[1]
  project_export = sys.argv[2]
  project_export_flag = sys.argv[3] #project_export[0] + project_export[1:].find()
  dict = {'name' : project_name, 
    'PyTemplateProjectName' : project_name, 
    'PyTemplateProjectNameLower' : project_name.lower(), 
    'PyTemplateProjectNameUpper': project_name.upper(), 
    'PyTemplateProjectNameExport' : project_export, 
    'PyTemplateProjectNameExportFlag' : project_export_flag}

  create_structure(project_name, dict)