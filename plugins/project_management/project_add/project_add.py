﻿#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud


import sys, os, stat, re, codecs, shutil
import win32com.client
from time import sleep

pj = os.path.join
sys.path.append(pj(os.path.split(__file__)[0], os.path.pardir, "common"))

print 'Loading file', __file__
print sys.version
print
print


import common as YYC


def openMainSolution(root_path):
	"""Returns the main solution"""
	
	root_path = os.path.abspath(root_path)
	print u'Opening solution in path', root_path

	instance = YYC.getVisualStudioInstance()
	soln = None
	try:
		soln = instance.Solution
		soln.Open(os.path.abspath(root_path))
		#soln.AddSolutionFolder("prout prout")
	
	except Exception, e:
		#visual_studio_instance.Quit()
		print 'Exception raised during the solution opening', e
		#raise Exception('Error while opening the solution')
	
	
	return soln

def solutionProjects(soln):
	"""Returns the names of the projects that are inside the solution given in argument"""
	l = []
	
	for i in range(soln.Projects.Count):
		try:
			l.append(soln.Projects.Item(i+1).Name)
		except Exception, e:
			print "Following exception occured while enumerating projects"
			print "from the solution", soln
			print e
	return l

def solutionAddProject(soln, project_file, solution_dir = None):
	"""Add a project to the solution. If "solution_dir" is a string, add the project into a folder inside the root of the solution"""
	projects = solutionProjects(soln)
	s2 = soln
	if(not solution_dir is None):
		if(not (solution_dir in projects)):
			s2 = soln.AddSolutionFolder(solution_dir).Object
		else:
			s2 = soln.Projects.Item(projects.index(solution_dir) + 1).Object

	try:
		print "Adding project", project_file
		s2.AddFromFile(project_file)
		pass
		
	except Exception, e:
		print "Following exception occured while adding project"
		print project_file
		print "to the current solution"
		print e
	return

def solutionConfigurations(soln):
	"""Returns the configurations of the current solution"""
	l = []
	for i in range(soln.SolutionBuild.SolutionConfigurations.Count):
		l.append(soln.SolutionBuild.SolutionConfigurations.Item(i+1).Name)
	return l

def getSolutionConfigurationFromName(soln, current):
	solution_configuration = None
	
	for i in range(soln.SolutionBuild.SolutionConfigurations.Count):
		current_configuration = soln.SolutionBuild.SolutionConfigurations.Item(i+1)
		if(current_configuration.Name == current):
			solution_configuration = current_configuration
			break
	
	if(solution_configuration is None):
		print "Solution's configuration named '%s' not found !" % current
	
	return solution_configuration



# GUID for objects
solution_folder_guid 	  = "{66A26720-8FB5-11D2-AA7E-00C04F688DDE}"
visual_cp_project_guid	= "{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}"
visual_cS_project_guid	= "{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}"

def returnProjectEngine(soln):
	return soln.Projects.Item(1).ProjectItems.Item(1).Object.Object.VCProjectEngine
	#soln.Projects.Item(1).ProjectItems.Item(3).Object.Object.Configurations.Item(1).OutputDirectory
	#soln.Projects.Item(1).ProjectItems.Item(3).Object.Object.Configurations.Item(1).Tools.Item(1).ToolKind



def getProjectsFromSolutionFolder(solution_folder, path = []):
	"""Special get from solution folders (cast seems not working properly)"""
	ret = []
	temp = path
	temp.append(solution_folder.Name)
	for i in range(solution_folder.ProjectItems.Count):
		if(solution_folder.ProjectItems.Item(i+1).Kind == solution_folder_guid):
			ret.extend(getProjectsFromSolutionFolder(solution_folder.ProjectItems.Item(i+1), temp))
		else:
			ret.append((solution_folder.ProjectItems.Item(i+1).Object, temp))

	return ret


def returnVCProjects(soln):
	"""Returns all the project that are not folders, recurse on folders"""
	ret = []
	for i in range(soln.Projects.Count):
		current_project = soln.Projects.Item(i + 1)
		
		if(current_project.Kind == solution_folder_guid):
			#print 'Recuse in folder', current_project.Name
			ret.extend(getProjectsFromSolutionFolder(current_project, []))
		else:
			ret.append((current_project.Object, []))
	
	return ret

def GetProjectFromUniqueName(projects, unique_name):
	for p in projects:
		if(p[0].UniqueName == unique_name): return p[0]
	return None


def PrintAllProjectInSolution(projects):
	
	l1, l2 = 0, 0
	
	for i in projects:
		l1 = max(l1, len(i[0].Name))
		l2 = max(l2, len(i[0].UniqueName))

	for i in projects:
		print (('Projects name : %%-%us') % (l1 + 5)) % i[0].Name,
		print (('Unique Name : %%-%us') % (l2 + 5)) % i[0].UniqueName,
		print 'Item Name', i[0].Object.ItemName
		print '\tItem path', i[1]
	
	return


def solutionParseOutputDirectories(soln):
	"""Report output directories of the projects inside the current solution"""
	
	DTE 				= YYC.getVisualStudioInstance()
	configurations 		= solutionConfigurations(soln)
	solution_projects 	= returnVCProjects(soln)
	
	out = {}
	
	for current in configurations:
		
		print 'Parsing configuration tree for ', current
		
		solution_configuration = getSolutionConfigurationFromName(soln, current)
		if(solution_configuration is None):
			continue
		
		out[current] = {}
		
		solutions_contexts = solution_configuration.SolutionContexts
		projects_selected = ""
		
		# for all solution's configurations
		for i in range(solutions_contexts.Count):
			
			current_context = solutions_contexts.Item(i + 1)
			
			if(current_context.ShouldBuild == False):
				print 'Context', current_context.Name, 'not selected for build'
				continue
			
			projects_selected += current_context.ProjectName + '\n'
			
			
			current_project = GetProjectFromUniqueName(solution_projects, current_context.ProjectName)
			if(current_project is None):
				print 'Project \'%s\' not found' % current_context.ProjectName
				continue


			out[current][current_context.ProjectName] = {}
			
			project_engine 	= current_project.Object.VCProjectEngine
			outdir 			= None
			
			project_configurations = current_project.Object.Configurations
			for c in range(project_configurations.Count):
				if(current_context.ConfigurationName == project_configurations.Item(c+1).ConfigurationName):
					configuration = project_configurations.Item(c+1)
					
					out[current][current_context.ProjectName]['out_dir'] 		  = os.path.abspath(configuration.Evaluate("$(OutDir)"))
					out[current][current_context.ProjectName]['out_dir_macro'] = configuration.OutputDirectory
					out[current][current_context.ProjectName]['int_dir'] 		  = os.path.abspath(configuration.Evaluate("$(IntDir)"))
					out[current][current_context.ProjectName]['int_dir_macro'] = configuration.IntermediateDirectory
					out[current][current_context.ProjectName]['log'] 			    = os.path.abspath(configuration.Evaluate(configuration.BuildLogFile))
					out[current][current_context.ProjectName]['log_macro'] 	  = configuration.BuildLogFile
					out[current][current_context.ProjectName]['output'] 		    = os.path.abspath(configuration.Evaluate("$(TargetPath)"))
					out[current][current_context.ProjectName]['output_macro'] 	= configuration.PrimaryOutput
					out[current][current_context.ProjectName]['int_dir_macro'] = configuration.IntermediateDirectory
					
			if 0:
				for tool in project_engine.Tools:
					print tool.ToolKind
					print project_engine.Evaluate("$(OutDir)")
			
			if 0:	
				for j in range(project.Configurations.Count):
					if(project.Configurations.Item(j + 1).Name == current_context.ProjectName):
						print ToolKind
						
	
		print 
		print "Selected projects are"
		print projects_selected		
	return out

def CheckOutputDirectories(configuration_tree):
	"""Checks the ouput directories"""
	
	for configuration_name in configuration_tree.keys():
		
		for project_name in configuration_tree[configuration_name].keys():

			# repertoire de sortie intermediaire
			if(configuration_tree[configuration_name][project_name]['int_dir'].lower().count(YYC.YAYI_TEMP_DIR.lower()) == 0):
				print 'Project %s | %s:\n\ttemporary output dir does not point to the intermediate directory' % (project_name, configuration_name)
				print '\tIntermediate directory points to ', configuration_tree[configuration_name][project_name]['int_dir']
				print '\tShould point inside ', YYC.YAYI_TEMP_DIR
				print '\tMacro was set to ', configuration_tree[configuration_name][project_name]['int_dir_macro']
				print 
			
			if(configuration_tree[configuration_name][project_name]['int_dir_macro'].count("$(ProjectName)") == 0):
				print 'Project %s | %s:\n\tconsider adding project name for temporary output dir' % (project_name, configuration_name)
				print 

			if(configuration_tree[configuration_name][project_name]['int_dir_macro'].count("$(ConfigurationName)") == 0):
				print 'Project %s | %s:\n\tconsider adding project name for temporary output dir' % (project_name, configuration_name)
				print 
			
			if(configuration_tree[configuration_name][project_name]['out_dir'].lower().count(YYC.YAYI_BIN_DIR.lower()) == 0):
				print 'Project %s | %s:\n\toutput dir does not point to the output directory' % (project_name, configuration_name)
				print '\tOutput directory points to ', configuration_tree[configuration_name][project_name]['out_dir']
				print '\tShould point inside ', YYC.YAYI_BIN_DIR
				print '\tMacro was set to ', configuration_tree[configuration_name][project_name]['out_dir_macro']
				print 

			if(configuration_tree[configuration_name][project_name]['out_dir_macro'].count("$(ConfigurationName)") == 0):
				print 'Project %s | %s:\n\tconsider adding project configuration for output directory' % (project_name, configuration_name)
				print 

def RepairDirectoriesAndConfigurations(configurtion_tree):
  """Repairs the default project in order to have them pointing to the right places. Should add the configurations also."""



def BuildVisualStudioSolution(solutionFullPath, defaultSolutionName, postBuildTask, convenientName):
	"""Launch Visual Studio and performs the build of each modules"""
	import win32com.client
	
	global global_output_directory
	global creation_time
	
	return_info 	= [convenientName]
	# Enforce Visual Studio 7.1 Global Interface VS Visual Studio 8
	DTE 			= win32com.client.Dispatch("VisualStudio.DTE.7.1")
	
	# Solution Object
	soln 			= DTE.Solution
	try:
		print u"Opening solution ", solutionFullPath
		soln.Open(solutionFullPath)
	except:
		DTE.Quit()
		raise Exception('Error while opening the solution')
	
	# solution build object
	sb 				= soln.SolutionBuild
	
	# All configurations
	dteConfigs 		= sb.SolutionConfigurations
	print u'Solution\'s configurations are:'
	for i in dteConfigs:
		print '\t', i.Name,
	print 
	
	# All projects
	projects = soln.Projects
	print u'Solution\'s projects are:'
	for i in projects:
		print u'\t', i.Name,
	print

	## Place the morphee directory (temporary folder) as the first include directory
	#_includeDirsOld = projects.Item(1).Object.Platforms(1).IncludeDirectories
	#print 'Inserting the temporary directory as the first include path :'
	#_includeDirs = temporary_dir + u";" + _includeDirsOld
	#print "\t", _includeDirs
	#projects.Item(1).Object.Platforms(1).IncludeDirectories = _includeDirs
	
	
	##cfgs	= projects.Item(1).Object.Configurations
	##cfg		= cfgs.Item(1)
	##tool	= cfg.Tools("VCCLCompilerTool")
	##ret		= tool.FullIncludePath
	##print 'Include path', ret
	
	print 
	print u'Build task...'
	if(defaultSolutionName != "all"):
		# all signfie qu'on se sert de la configuration de la solution pour faire le build
		# sinon on fait la compilation séparement par projet
		
		# on cherche le projet principal à partir duquel on va construire la solution (dépendance des autres projets dans la solution)
		j = None
		for i in projects:
			_str = str(i.Name)
			if(_str.lower().find(str(defaultSolutionName).lower()) != -1):
				j = i
				break
		
		if(j is None):
			print "Warning, the given project (%s) wasn't found in the solution" % defaultSolutionName
			j = projects.Item(1)
		
		
		print u'Defining ', j.Name, u' as the active project'
		sb.StartupProjects = j
		
		
		# construction de toutes les configurations
		for config in dteConfigs:
			if(str(config.Name).lower().find('dll') == -1):
				continue
			
			#if(	(str(config.Name).lower() != str("Release Multithreaded No Global Optimizations DLL").lower())
			#	and 
			#	(str(config.Name).lower() != str("Release Multithreaded Pentium4 No Global Optimizations DLL").lower())):
			#	continue
			#if(str(config.Name).lower() != str("Release Multithreaded").lower()):
			#	continue
			#if(str(config.Name).lower() != str("Debug Multithreaded DLL").lower()):
			#	continue

			dict_config = {'ConfigurationName': str(config.Name) }
			print u'-> Building configuration ', dict_config['ConfigurationName']
			config.Activate()
			
			return_info.append(dict_config)
			
			# building the solution
			t1 = datetime.datetime.now()
			sb.Build(True)
			t2 = datetime.datetime.now()
			
			if(sb.LastBuildInfo == 0):
				print u'\tBuild successfull'
				dict_config['IsSuccess'] = True
			else:
				dict_config['IsSuccess'] = False
				print u'\tBuild not successfull'
				#continue
				
			print u'\tBuild time : %.2dh %.2dm %2ds' % ((t2-t1).seconds/ 3600, ((t2-t1).seconds % 3600) / 60, (t2-t1).seconds % 60)
			dict_config['CompilationTime'] = (t2-t1).seconds
		
			# retrieving the ouput files list
			dict_config['projects'] = []
			for proj in projects:
				# au cas où ce serait un autre type de projet
				if(proj.Object is None):
					continue
				
				vcConfig = None
				for jj in range(proj.Object.Configurations.Count):
					
					j = proj.Object.Configurations.Item(jj+1)
					#print j.ConfigurationName, "\t", config.Name
					if(j.ConfigurationName == config.Name):
						vcConfig = j
						break
				
				if(vcConfig is None):
					raise u'Cannot find the configuration ', str(config.Name)
				
				intermediate_dir 	= vcConfig.Evaluate("$(IntDir)")
				output_dir			= vcConfig.Evaluate("$(TargetDir)")
				output_files		= [vcConfig.Evaluate("$(TargetPath)")]
				
				_logFile 			= file(os.path.join(intermediate_dir, "BuildLog.htm"))
				_log 				= _logFile.read(-1)
				_logFile.close()
				
				if(str(vcConfig.ConfigurationName).lower().find("dll") != -1):
					output_files.append(os.path.splitext(output_files[0])[0] + '.lib')
					output_files.append(os.path.splitext(output_files[0])[0] + '.exp')		
				
				dict_config['projects'].append({'output_files' : output_files, 'name': proj.Name, 'log' : _log, 
					'intermediate_dir':intermediate_dir,
					'output_dir':output_dir})
			
			
		
	else:
	
		# construction de tous les projets, pris séparement
		for config in dteConfigs:
			if(str(config.Name).lower().find('dll') == -1):
				continue


			#if(	(str(config.Name).lower() != str("Release Multithreaded No Global Optimizations DLL").lower()) 
			#	and 
			#	(str(config.Name).lower() != str("Release Multithreaded Pentium4 No Global Optimizations DLL").lower())):
			#	continue
			#if(str(config.Name).lower() != str("Release Multithreaded").lower()):
			#	continue
			#if(str(config.Name).lower() != str("Debug Multithreaded DLL").lower()):
			#	continue

			dict_config = {'ConfigurationName':str(config.Name)}
			
			print u'-> Building configuration ', dict_config['ConfigurationName']
			config.Activate()
			
			return_info.append(dict_config)
			
			# ici build de tous les projets
			allAreSuccess = True
			dict_config['projects'] = []
			
			
			for proj in projects:
			
				if(proj.Object is None):
					continue
				
				print u'\tBuilding project', proj.Name
				
				# looking for correct configuration
				vcConfig = None
				for jj in range(proj.Object.Configurations.Count):
					j = proj.Object.Configurations.Item(jj+1)
					
					trimed_name = j.ConfigurationName#[:j.ConfigurationName.find('|')]
					#print '\t\t', trimed_name
					if(trimed_name.lower() == str(config.Name).lower()): 
						vcConfig = j
						break
				
				if(vcConfig is None):
					print '\tProject configuration not found for project', proj.Name
					continue
				
				#print u'\tProject kind', proj.Kind
				#sb.StartupProjects = proj
				
				# building 'specifically' the project
				t1 = datetime.datetime.now()
				
				# ca ca marche bien, mais on va plutot utiliser l'objet solutionBuild
				if 1:
					try:
						vcConfig.Build()
						print '\t'
						while(sb.BuildState != 3):
							print '.',
							sleep(2)
						print
					except Exception, e:
						print '\tException occured while building', proj.Name, ' : ', e
				
				# ben du coup ca ca ne marche pas
				if 0:
					try:
						sb.BuildProject(str(config.Name), str(proj.Name), True)
					except Exception, e:
						print '\tException occured while building', proj.Name, ' : ', e
				
				t2 = datetime.datetime.now()
			
				if(sb.LastBuildInfo != 0):
					print u'\t\tBuild not successfull'
					allAreSuccess = False
				else:
					print u'\t\tBuild successfull'
					
				
				print u'\t\tBuild time : %.2dh %.2dm %2ds' % ((t2-t1).seconds / 3600, ((t2-t1).seconds % 3600) / 60, (t2-t1).seconds % 60)
				dict_config['CompilationTime'] = (t2-t1).seconds

				# output files
				intermediate_dir 	= vcConfig.Evaluate(vcConfig.IntermediateDirectory)
				output_dir			= vcConfig.Evaluate(vcConfig.OutputDirectory)
				output_files		= [vcConfig.Evaluate("$(TargetPath)")]
				
				_logFile 			= file(os.path.join(intermediate_dir, "BuildLog.htm"))
				_log 				= _logFile.read(-1)
				_logFile.close()
				
				if((str(vcConfig.ConfigurationName).lower().find("dll") != -1) and (postBuildTask != "run")):
					output_files.append(os.path.splitext(output_files[0])[0] + '.lib')
					output_files.append(os.path.splitext(output_files[0])[0] + '.exp')
		
				dict_config['projects'].append({'output_files' : output_files, 'name': proj.Name, 'log' : _log, 'IsSuccess' : sb.LastBuildInfo == 0,
					'intermediate_dir':intermediate_dir,
					'output_dir':output_dir})
				#break

			dict_config['IsSuccess'] = allAreSuccess
			if(dict_config['IsSuccess']):
				print u"-> All projects' build were successfull"
			else:
				print u"-> Error on some projects' build"
			
			##break ## pour le moment on ne fait qu'une seule configuration



	# Post build task
	print 
	print u'Post build tasks...'
	for my_dict in return_info[1:]:

		print 
		
		if(postBuildTask == "archive"):
			if(not my_dict['IsSuccess']):
				continue
			#print my_dict
			
			archive_file_name = "%s - %s - %.4d-%.2d-%.2d - %.2d-%.2d.tar.bz2" % (	convenientName, 
						my_dict['ConfigurationName'], 
						creation_time.year, creation_time.month, creation_time.day, creation_time.hour , creation_time.minute) 
			archive_file_name_dir = os.path.join(web_output_directory, archive_file_name)
			
			_filesToArchive = []
			for proj in my_dict['projects']:
				_filesToArchive.extend(proj['output_files'])
			
			print u'Creating archive for configuration %s' % my_dict['ConfigurationName']
			ArchiveOutputFiles(archive_file_name_dir, _filesToArchive)
			
			my_dict['ArchiveName']		= archive_file_name
			my_dict['Sha1HexDigit'] 	= computeSHA1keys([archive_file_name_dir])[0].hexdigest() ## suppose qu'un seul fichier
			#print my_dict['Sha1HexDigit']
			
			## nettoyage des fichiers ancients
			_list = os.listdir(web_output_directory) 
			_index= [_list.index(i) for i in _list if i.find("%s - %s - " % (convenientName,my_dict['ConfigurationName'])) != -1 and os.path.splitext(i)[1] == '.bz2']
			for i in _index:
				if(_list[i] != my_dict['ArchiveName']):
					print u'\tRemoving old file "%s"'% _list[i]
					os.remove(os.path.join(web_output_directory, _list[i]))

			
		elif (postBuildTask == "run"):
			## on archive qd meme les fichiers générés (exactement de la meme manière qu'au dessus)

			archive_file_name = "%s - %s - %.4d-%.2d-%.2d - %.2d-%.2d.tar.bz2" % (	convenientName, 
						my_dict['ConfigurationName'], 
						creation_time.year, creation_time.month, creation_time.day, creation_time.hour , creation_time.minute) 
			archive_file_name_dir = os.path.join(web_output_directory, archive_file_name)
			
			_filesToArchive = []
			for proj in my_dict['projects']:
				if(proj['IsSuccess']):
					_filesToArchive.extend(proj['output_files'])
			
			print u'Creating archive for configuration %s' % my_dict['ConfigurationName']
			ArchiveOutputFiles(archive_file_name_dir, _filesToArchive)
			
			my_dict['ArchiveName'] = archive_file_name
			my_dict['Sha1HexDigit'] = computeSHA1keys([archive_file_name_dir])[0].hexdigest() ## suppose qu'un seul fichier
			#print my_dict['Sha1HexDigit']
			
			## nettoyage des fichiers ancients
			_list = os.listdir(web_output_directory) 
			_index= [_list.index(i) for i in _list if i.find("%s - %s - " % (convenientName,my_dict['ConfigurationName'])) != -1 and os.path.splitext(i)[1] == '.bz2']
			for i in _index:
				if(_list[i] != my_dict['ArchiveName']):
					print u'\tRemoving old file "%s"'% _list[i]
					os.remove(os.path.join(web_output_directory, _list[i]))

			## on execute et on log la sortie
			for proj in my_dict['projects']:
				if(not proj['IsSuccess']):
					continue
				
				if(len(proj['output_files']) > 1):
					print u'WARNING: considering only the first file of ', proj['output_files']
				#print os.path.splitdrive(proj['output_files'][0])
				os.chdir(os.path.splitdrive(proj['output_files'][0])[0])	# pour la partition
				os.chdir(os.path.split(proj['output_files'][0])[0])	# pour le directory
				print u'\tExecuting test case of %s ...' % proj['name']
				u,v,w = os.popen3(os.path.split(proj['output_files'][0])[1] + " --detect_memory_leak=0") #os.popen3(os.path.split(proj['output_files'][0])[1] + " --log_level=messages")
				# problème av python, d'abord lire stderr puis stdout
				proj['testcaseErr'] = w.readlines()
				proj['testcaseOut'] = v.readlines()
				w.close()
				v.close()
				u.close()

				print u'\tExecution ok'
				
			
	
	

	## put back the configuration dirs
	#projects.Item(1).Object.Platforms(1).IncludeDirectories = _includeDirsOld
	soln.Close()
	DTE.Quit()

	return return_info


if __name__ == '__main__':
	main_solution = pj(YYC.YAYI_ROOT_DIR, "core", "yayi.sln")
	soln = openMainSolution(main_solution)
	
	PrintAllProjectInSolution(returnVCProjects(soln))
	
	
	#print solutionProjects(soln)
	#print solutionConfigurations(soln)
	
	#vcproj_files = YYC.get_files(YYC.YAYI_ROOT_DIR, 'vcproj')[0]
	
	#for f in vcproj_files:
	#	solutionAddProject(soln, f, u"core")
	
	#soln.SaveAs(main_solution)
	
	configurations_tree = solutionParseOutputDirectories(soln)
	
	print configurations_tree['Debug']['yayiArithmetic\yayiArithmetic.vcproj']['int_dir']
	
	CheckOutputDirectories(configurations_tree)
	
	#print 'Configuration tree', configurations_tree
	
	soln.Close()
	
	print 'Script loaded with arguments', sys.argv[1:]
	
	
	
	


