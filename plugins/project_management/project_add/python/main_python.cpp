
#include <Yayi/python/yayi${PyTemplateProjectName}Python/${PyTemplateProjectNameLower}_python.hpp>


#include <boost/python/ssize_t.hpp>
#include <boost/python/module.hpp>
#include <boost/python/detail/none.hpp>

namespace bpy = boost::python;


BOOST_PYTHON_MODULE( Yayi${PyTemplateProjectName}Python )
{
}
