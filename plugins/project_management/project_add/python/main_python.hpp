#ifndef ${PyTemplateProjectNameUpper}_PYTHON_HPP__
#define ${PyTemplateProjectNameUpper}_PYTHON_HPP__

#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/def.hpp>

namespace bpy = boost::python;

#include <Yayi/core/yayiCommon/common_types.hpp>
#include <Yayi/core/yayiImageCore/include/yayiImageCore.hpp>
#include <Yayi/core/yayi${PyTemplateProjectName}/yayi${PyTemplateProjectName}.hpp>


#endif /* ${PyTemplateProjectNameUpper}_PYTHON_HPP__ */
