#ifndef YAYI_${PyTemplateProjectNameUpper}_MAIN_HPP__
#define YAYI_${PyTemplateProjectNameUpper}_MAIN_HPP__



#include <Yayi/core/yayiCommon/common_config.hpp>


#ifdef YAYI_EXPORT_${PyTemplateProjectNameExport}_
#define Y${PyTemplateProjectNameExportFlag}_ MODULE_EXPORT
#else
#define Y${PyTemplateProjectNameExportFlag}_ MODULE_IMPORT
#endif



#endif /* YAYI_${PyTemplateProjectNameUpper}_MAIN_HPP__ */

