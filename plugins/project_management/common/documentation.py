#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud


def compileDoxygenHelpSystem(modulename, docname, output_name):
  """Creates the doxygen help. Launches also the HTML Help creator oin windows plateform.
  The help destination root path is : %web_output_directory%\\documentation
   - the HTML Help will will go there
   - the doxygen files will be moved to .\\doxygen"""
   

  # we should change the current directory to the one of the root document
  # see if newer versions of doxygen fix this problem
  docname= pj(temporary_dir, modulename, docname)
  docdir = os.path.split(docname)[0]
  os.chdir(docdir)

  print docdir
  print doxygenExecutable, " " + docname


  # extraction du répertoire de destination à partir de la configuration de doxygen
  _configFile = file(docname)
  stringFile	= _configFile.read(-1)
  _configFile.close()

  _index1 = stringFile.find("OUTPUT_DIRECTORY")
  if(_index1 == -1):
    print u'Documentation not generated'
    return  ""


  _outputDir = str(stringFile[_index1: stringFile.find("\n", _index1)]).split("=")[1].lstrip().rstrip()
  _outputDir = os.path.abspath(os.path.join(docdir, _outputDir, 'html'))

  # parce que ce blaireau de doxygen n'est pas capable de me dire qu'il se chie dessus à cause de l'absence du répertoire de destination
  if(not os.path.exists(_outputDir)):os.makedirs(_outputDir)

  # lancement de doxygen
  os.spawnl(os.P_WAIT, doxygenExecutable, " " + docname)#"\"" + str(os.path.join(docdir, docname)) + "\""
	
	
	
  # lancement de HTML HELP (WIN98)
  if(sys.platform.lower().find('win') != -1):
    os.spawnl(os.P_WAIT, htmlHelpWorkShopExe, " " + os.path.join(_outputDir, "index.hhp"))
    web_chm_file = os.path.join(web_output_directory, "documentation", output_name, output_name + ".chm")
    if(os.path.exists(web_chm_file)): os.remove(web_chm_file)
    if(not os.path.exists(os.path.split(web_chm_file)[0])): os.makedirs(os.path.split(web_chm_file)[0])
    shutil.move(os.path.join(_outputDir, "index.chm"), web_chm_file)
	
  # déplacement des répertoires
  doxygen_output_path = os.path.join(web_output_directory, "documentation", output_name, "doxygen")
  shutil.rmtree(doxygen_output_path, ignore_errors = True)
  #if(not os.path.exists(doxygen_output_path)): os.makedirs(doxygen_output_path)
  shutil.move(_outputDir, doxygen_output_path)

  # log des résultats
  _strHtmlHelp  = WriteTag("name", output_name + " Html Help")
  _strHtmlHelp += WriteTag("url", "documentation/%s/%s.chm" % (output_name, output_name))
  _strHtmlHelp  = WriteTag("forme", _strHtmlHelp)

  _strHtmlHelp2  = WriteTag("name", output_name + " Html")
  _strHtmlHelp2 += WriteTag("url", "documentation/%s/doxygen/index.html" % output_name)
  _strHtmlHelp2  = WriteTag("forme", _strHtmlHelp2)

  # est-ce qu'on a besoin du log de doxygen ???
  return WriteTag("document", WriteTag("name", modulename) + WriteTag("formes", _strHtmlHelp + _strHtmlHelp2))



