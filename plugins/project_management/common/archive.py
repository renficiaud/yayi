﻿#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud

import os, types, stat, hashlib

def ArchiveFilesTar(archive_file_name, file_list, compression_type = 'bz2'):
  """Create a tar.bz2 archive from the input file list"""
  if(compression_type not in ['bz2', 'gz', 'zip']):
    raise Exception("Unknown compression method")
	
  if(compression_type in ['bz2', 'gz']):
    import tarfile as TAR
    my_archive = TAR.open(archive_file_name, 'w:%s' % compression_type) 
    current_function_pointer = my_archive.add
	
  else:
    assert(compression_type == 'zip')
    import zipfile as ZIP
    my_archive = ZIP.ZipFile(archive_file_name, 'w')
    current_function_pointer = my_archive.write

  for _file in file_list:
    print 'adding', _file, 'to tar'
    #file_name = os.path.split(_file)[1]
    if(type(_file) is types.UnicodeType):
      current_function_pointer.__call__(_file.encode('latin_1'))#, file_name)
    else:
      current_function_pointer.__call__(_file)#, file_name)
    #my_archive.add(_file)
  my_archive.close()
  return


def Checksum(current_file_name, check_type = 'sha512'):
  """Computes the sha1 key for each files in list_of_files_name. For web information purposes"""
  size_block = 1024 * 1024 # taille du block de lecture

  d = {'sha1' : hashlib.sha1, 'md5': hashlib.md5, 'sha512': hashlib.sha512}
  
  if(not d.has_key(check_type)):
    raise Exception("Unknown checksum method")

  file_size = os.stat(current_file_name)[stat.ST_SIZE]
  o_file_size	= file_size
  f = file(current_file_name, 'rb')

  key = d[check_type].__call__()

  while True:
    s = f.read(size_block)
    key.update(s)
    file_size -= size_block
    if(len(s) < size_block):
      break
  f.close()

  return key.hexdigest().upper()


