﻿#!/usr/bin/python
# -*- coding: utf_8 -*-
# (c) 2007-2015 Raffi Enficiaud

import os, sys, types

def get_files(root_path, recursive = True):
    """Returns all the files under root_path, which extension is in extensions"""
    l = []
  
    if recursive:
        for root, dirs, files in os.walk(root_path, topdown = True):
            l += [os.path.join(root, i) for i in files]
    else:
        l = os.listdir(root_path)
    
    return l


def WriteTag(tagname, content):
  """Writes a tag into an XML stream"""
  tagname = unicode(tagname)
  return u"\n<" + tagname + u">\n" + unicode(content) + u"\n</" + tagname + u">\n"

