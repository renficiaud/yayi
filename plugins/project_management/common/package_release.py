#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud


import os, sys, time, shutil, ftplib
import common as CC, add_licence as AL, archive as AR, website_management as WM

cmake_build_file    = 'CMakeLists.txt'
files_to_add        = ['.cpp', '.hpp', cmake_build_file, '.py']
files_need_licence  = files_to_add

def directories_to_add(root_path = CC.YAYI_ROOT_DIR):
  return [(os.path.join(root_path, 'core'),      True,   files_to_add), 
          (os.path.join(root_path, 'coreTests'), True,   files_to_add), 
          (os.path.join(root_path, 'python'),    True,   files_to_add), 
          (os.path.join(root_path, 'build'),     False,  [cmake_build_file, 'CMakeCompilerSpecific.txt', 'CMakeOptimizationLib.txt', '.py']),
          (root_path,                            False,  ['LICENSE_1_0.txt']),
          (os.path.join(root_path, 'plugins', 'external_libraries'),           False,   [cmake_build_file, '.gz']),
          (os.path.join(root_path, 'plugins', 'external_libraries', 'Config'), False,   None),
          (os.path.join(root_path, 'coreTests', 'yayiTestData'),               False,   ['.jpg', '.png', '.h5'])
        ]

def directories_to_add_without_licence(root_path = CC.YAYI_ROOT_DIR):
  return [ (os.path.join(root_path, 'plugins', 'external_libraries', 'boost-1_38_fix'), True,   files_to_add) ]

files_to_check        = [ os.path.join(CC.YAYI_ROOT_DIR, 'core', 'yayiCommon', 'src', 'current_configuration_svn_generated.hpp'),
                          os.path.join(CC.YAYI_ROOT_DIR, 'build', 'CMakeCompilerSpecific.txt') 
                        ]

website_access        = {'site': 'ftpperso.free.fr', 'user' : 'raffi.enficiaud'}


# format of the release date
release_date          = "release_%.4d_%.2d_%.2d" % (time.localtime()[0], time.localtime()[1], time.localtime()[2])

# destination of the release temporary files
dest_dir_root         = os.path.join(CC.YAYI_TEMP_DIR, release_date, 'Yayi')


def getAllFiles(dirs = directories_to_add()):
  """Retrieve all the files to be archived"""
  lfiles = []
  for d in dirs:
    l = CC.get_files(d[0], d[2], d[1])
    lfiles.extend(l[0])
    
  return lfiles

def copyFileWithLicence(l, can_have_licence = True, root_path = CC.YAYI_ROOT_DIR):
  """Copy the files to be archived to another place (without the .svn). During the copy
  adds the license information for the relevant files."""
  out = []

  if not os.path.exists(dest_dir_root):
    os.makedirs(dest_dir_root)
  
  for f in l:
    rel_path = CC.getRelativePath(f, root_path)
    dest_file = os.path.abspath(os.path.join(dest_dir_root, rel_path))
    dest_dir = os.path.split(dest_file)[0]
    
    if not os.path.exists(dest_dir):
      os.makedirs(dest_dir)
    
    #print 'copying file', f, 'to', dest_file
    
    need_licence = False
    if os.path.splitext(f)[1] in files_need_licence:
      need_licence = True
    elif os.path.split(f)[1] in files_need_licence:
      need_licence = True
    
    need_licence &= can_have_licence
    
    if need_licence:
      print 'Adding licence to', dest_file
      u = AL.add_licence(f)
      if(os.path.exists(dest_file)):
        os.remove(dest_file)
      fo = file(dest_file, "w")
      fo.writelines(u)
      fo.close()
    
    else:
      print 'Copying to', dest_file
      shutil.copyfile(f, dest_file)

    out.append(dest_file)
    
  return out

def copyFileAndRemoveLicence(l, destination_folder, can_have_licence = True, root_path = CC.YAYI_ROOT_DIR):
  """Copy the files to be archived to another place (without the .svn). During the copy
  adds the license information for the relevant files."""
  out = []
  
  if not os.path.exists(destination_folder):
    os.makedirs(destination_folder)
  
  for f in l:
    rel_path = CC.getRelativePath(f, root_path)
    dest_file = os.path.abspath(os.path.join(destination_folder, rel_path))
    dest_dir = os.path.split(dest_file)[0]
    
    if not os.path.exists(dest_dir):
      os.makedirs(dest_dir)
    
    
    need_licence = False
    if os.path.splitext(f)[1] in files_need_licence:
      need_licence = True
    elif os.path.split(f)[1] in files_need_licence:
      need_licence = True
    
    need_licence &= can_have_licence
    
    if need_licence:
      print 'Remove license from', dest_file
      u = AL.del_licence(f, root_path = root_path)
      if(os.path.exists(dest_file)):
        os.remove(dest_file)
      fo = file(dest_file, "w")
      fo.writelines(u)
      fo.close()
    
    else:
      print 'Copying to', dest_file
      shutil.copyfile(f, dest_file)

    out.append(dest_file)
    
  return out





def ArchiveAll(l):
  """Puts all the files into an archive"""
  output_dir = os.path.join(dest_dir_root, os.path.pardir)
  output_file = os.path.join('yayi_' + release_date + '.tar.bz2')
  
  rel = []
  for f in l:
    rel.append(CC.getRelativePath(f, output_dir))
  
  current_dir = os.path.realpath(os.getcwd())
  
  os.chdir(output_dir)
  
  if(os.path.exists(output_file)):
    os.remove(output_file)
  AR.ArchiveFilesTar(output_file, rel, 'bz2')
  
  digest = AR.Checksum(output_file, 'sha1')
  
  os.chdir(current_dir)
  print digest
  
  return (os.path.join(output_dir, output_file), digest)









def transfert_archive(archive, digest, ftp_site):
  """Transfer the archive (by ftp) to the remote server"""
  
  pass_ = None
  if((not ftp_site.has_key('pass')) or (ftp_site['pass'] is None)):
    pass_ = raw_input('Please enter the password for the site %s@%s : ' % (ftp_site['user'], ftp_site['site']))
  else:
    pass_ = ftp_site['pass']
  
  ftp = None
  
  try:
    ftp = ftplib.FTP(ftp_site['site'], ftp_site['user'], pass_)
  except Exception, e:
    print 'An exception occurred during the connexion to %s@%s : verify the connexion parameters' % (ftp_site['user'], ftp_site['site'])
    raise e
  
  ftp.retrlines('LIST')
  
  ftp.storbinary('STOR %s' % os.path.split(archive)[1], open(archive, 'rb'))
  
  # verification ?
  # store multiple files ?
  
  
  ftp.retrlines('LIST')
  ftp.quit()
  
  
  # puts the password on 'cache'
  if((not ftp_site.has_key('pass')) or (ftp_site['pass'] is None)):
    ftp_site['pass'] = pass_
  
  return


  

def make_a_release(name = None):
  """Main function for doing all the operations for releasing the code on the website"""
  
  # just a check
  AllAreThere = True
  for f in files_to_check:
    if(not os.path.exists(f)):
      print "File", f, "does not exist"
      AllAreThere = False
  
  if(not AllAreThere):
    raise RuntimeError("Some files are missing, check your generation tree")
    
  
  l = getAllFiles(directories_to_add())
  ll= copyFileWithLicence(l)
  
  l1 = getAllFiles(directories_to_add_without_licence())
  ll1 = copyFileWithLicence(l1, False)

  ll.extend(ll1)

  t = ArchiveAll(ll)
  
  u = raw_input('Do you want to release this package on the Web Site ? [y/n] ')
  while u != 'y' and u != 'n':
    u = raw_input('Do you want to release this package on the Web Site ? [y/n] ')
  
  if(u == 'n'):
    return
  
  print 'Transfering archive... '
  transfert_archive(t[0], t[1], website_access)
  print 'Transfer ok'

  if(not(name is None)):
    print 'The release name is "%s"'% name
    u = raw_input("Is that correct ? [y/n]")
    if(u == 'y'):
      release_name = name
    else:
      name = None

  if(name is None):
    release_name = CC.get_from_keyboard_with_confirmation('release name')
  
  files_html = WM.do_the_website(t[0], t[1], release_name, release_date)
  
  for f in files_html:
    print 'Transferring ... ', f
    transfert_archive(f, '', website_access)


def remove_licence_info_from_a_release(directory_release, destination_folder):
  """Main function for doing all the operations for releasing the code on the website"""
  
  l = getAllFiles(directories_to_add(directory_release))
  ll= copyFileAndRemoveLicence(l, destination_folder, root_path = directory_release)
  
  l1 = getAllFiles(directories_to_add_without_licence(directory_release))
  ll1 = copyFileAndRemoveLicence(l1, destination_folder, can_have_licence = False, root_path = directory_release)

  return



if __name__ == '__main__':
  if(len(sys.argv) > 1): name = sys.argv[1]
  else: name = None
  
  make_a_release(name)
  
  
  #remove_licence_info_from_a_release(sys.argv[1], sys.argv[2])
  
  