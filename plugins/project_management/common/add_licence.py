#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud


import common as CC, os, sys, time

comment_characters = {
  '.cpp' : {'header' : '/*', 'newline': ' *', 'footer' : ' */', 'comment' : ('//', '/*')}, 
  '.hpp' : {'header' : '/*', 'newline': ' *', 'footer' : ' */', 'comment' : ('//', '/*')}, 
  '.py'  : {'header' : '#  -*- coding=UTF-8 -*-',  'newline': '#',  'footer' : '#'  , 'comment' : ('#')},
  '.txt' : {'header' : '#',  'newline': '#',  'footer' : '#'  , 'comment' : ('#')}
}

def filename_licence(root_path = CC.YAYI_ROOT_DIR): 
  return os.path.join(root_path, 'LICENSE_1_0.txt')

licence_header    = 'Copyright Raffi Enficiaud 2007-' + str(time.localtime()[0])
licence_content   = ('',
  'Distributed under the Boost Software License, Version 1.0.', 
  '(See accompanying file %(filename)s or copy at http://www.boost.org/LICENSE_1_0.txt)',
  '')
licence_delimiter = ' -:- ' + 'LICENCE' + ' -:- '


def format_licence(filename, root_path = CC.YAYI_ROOT_DIR):
  ext = os.path.splitext(filename.lower())[1]
  if(not comment_characters.has_key(ext)):
    raise Exception("The comment characters for filename \"" + filename + "\" has not been defined")

  d = comment_characters[ext]
  
  #print 'Relative filenames between', filename_licence, 'and', os.path.split(filename)[0]
  licence_file_relivative_path = CC.getRelativePath(filename_licence(root_path), os.path.split(filename)[0])
  
  new_line = '\n' + d['newline'] + ' '
  src = d['header'] + new_line + licence_delimiter
  src += new_line
  src += licence_header
  for s in licence_content:
    src += new_line
    if(s.find('%(filename)s') >= 0):
      src += s % {'filename': licence_file_relivative_path}
    else:
      src += s 

  src += new_line
  src += licence_delimiter + '\n' + d['footer'] + '\n\n'

  return unicode(src)

def add_licence(fn, encoding = 'utf8'):
  """Adds a licence to the content of a file"""
  
  f = file(fn, "r")
  lic = format_licence(fn)
  
  ext = os.path.splitext(fn.lower())[1]
  if(not comment_characters.has_key(ext)):
    raise Exception("The comment characters for filename \"" + filename + "\" has not been defined")

  d = comment_characters[ext]
  
  t_comment = d['comment']

  lic_added = False
  
  
  # on ajoute la licence au début et basta !
  o = ''
  o += lic.encode(encoding)
  lic_added = True
  
  
  s = unicode(f.readline(), encoding)
  while s != '':
    if(not lic_added):
      # Raffi: ce code peut potentiellement corrompre certaines sources commençant par un champ en commentaire /* */
      one_comment = False
      for dd in t_comment:
        ff = s.find(dd)
        if((ff > 0 and s[0:ff].strip() == '') or (ff == 0)):
          # ligne de commentaire
          one_comment = True
          break
      
      if(not one_comment):
        o += lic.encode(encoding)
        lic_added = True
    
    #print s, s.decode('utf8'), s.encode('utf8')
    #print s.encode('utf8'),
    o += s.encode(encoding)
    s = unicode(f.readline(), encoding)#.decode('utf-8')
    #print s
  
  return o

def del_licence(fn, root_path = CC.YAYI_ROOT_DIR, encoding = 'utf-8'):
  """Delete the licence from the content of a file"""  

  # on suppose que la licnce n'a pas changée en termes de nombre de lignes
  
  f = file(fn, "r")
  lic = format_licence(fn, root_path)
  
  ext = os.path.splitext(fn.lower())[1]
  if(not comment_characters.has_key(ext)):
    raise Exception("The comment characters for filename \"" + filename + "\" has not been defined")

  d = comment_characters[ext]
  
  t_comment = d['comment']
  
  # on ajoute la licence au début et basta !
  o = ''
  
  lic_removed = False
  nb_lines = len(lic.splitlines())
  
  s = unicode(f.readline(), encoding)
  current_count = 0
  current_line = 0
  while s != '':
    current_line += 1
    if(not lic_removed):
      if(current_count == 0):
        for u in lic.splitlines():
          if(s.strip() == u.strip()): 
            current_count += 1
            try:
              s = unicode(f.readline(), encoding)
            except Exception, e:
              print "Exception caught while reading line", current_line + current_count +1, "of file", fn
              print "Exception says:", e
              raise e
          else:
            break
        
            
      # Raffi: ce code peut potentiellement corrompre certaines sources commençant par un champ en commentaire /* */
      if(current_count == nb_lines):
        lic_removed = True
      elif (current_count > 2):
        raise Exception("Licence partially found for file '" + fn + "'")
      else:
        current_count = 0

    #s = unicode(f.readline(), encoding)

    o += s.encode(encoding)
    try:
      t = f.readline()
      s = unicode(t, encoding)#.decode('utf-8')
    except Exception, e:
      print "Exception caught while reading line", current_line + current_count +1, "of file", fn
      print "Line content is '", t, "'"
      print "Exception says:", e
      raise e
    
  return o




if __name__ == '__main__':
  print format_licence(__file__)
  print CC.getRelativePath(filename_licence(), os.path.split(__file__)[0])
  print os.path.exists(CC.getRelativePath(filename_licence, os.path.split(__file__)[0]))
  u = add_licence(__file__)
  print u
  
  print del_licence(add_licence(__file__), False)
  print del_licence(__file__)
  