#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) R. Enficiaud

import os, sys, time, shutil, ftplib, re
import common as CC, add_licence as AL, archive as AR


# Files to store on the website part
website_file          = [os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'HTMLPage.htm'), 'index.html']
other_web_site_files  = [ 
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'StyleSheet.css'),
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'Changes.html'),
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'History.html'),
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'HowToBuild.html'),
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'Licence.html'),
  os.path.join(CC.YAYI_ROOT_DIR, 'plugins', 'WebSite', 'SamplePage.html')
  ]


menu = [
  (website_file[1], 'The project'), 
  ('HowToBuild.html', 'HowToBuild ?'), 
  ('SamplePage.html', 'Samples page'), 
  ('History.html', 'History'), 
  ('Licence.html', 'Licence'), 
  ('Changes.html', 'Changes log')]

def do_menu(current_file):
  """Creates the menu accross the pages of the website"""
  s = ''
  
  for i in range(len(menu)):
    if(menu[i][0] == os.path.split(current_file)[1]):
      s += u"<font color=\"#cc5555\">&#9632;</font> "
    s += u"<a href=\"" + unicode(menu[i][0]) + u"\">" + unicode(menu[i][1]) + "</a>"
    
    if(i < (len(menu) - 1)):
      s += u" / "

  return s

def replace_tag_on_the_site(fn, tag_name, replace_value, do_remove_tag = False, insert_after_tag = True):
  """Adds the new file to the HTML page of the website"""
  
  f = file(fn, "r")
  
  new_added = False
  
  try:
    o = ''
    s = f.readline().decode('utf8')
    while s != '':
      if(not new_added):
        ff = s.find(tag_name)
        if(ff >= 0):

          if(do_remove_tag):
            o += s[0:ff].encode('utf8')
            o += replace_value.encode('utf8')
            o += s[(ff + len(tag_name)):].encode('utf8')
          
          else:
            if(insert_after_tag):
              o += s[:(ff+len(tag_name))].encode('utf8')
              o += replace_value.encode('utf8')
              o += s[(ff+len(tag_name)):].encode('utf8')
            else:
              o += s[:ff].encode('utf8')
              o += replace_value.encode('utf8')
              o += s[ff:].encode('utf8')
              
          new_added = True
          s = f.readline().decode('utf8')
          continue
        
      o+= s.encode('utf8')
      s = f.readline().decode('utf8')
      #print s

  except Exception, e:
    print "An exception occured during the parsing of the file:", e
    print "current line was:", s
    raise
  
  return o



def add_new_archive_to_the_site(archive_name, SHA, release_name, fn = website_file[0]):
  """Adds the new file to the HTML page of the website"""

  d_to_find = '<!-->automated release part<-->'
  info_to_add = '''
    <p>
      <ul>
        <li>Release '%s':<br>
          <a href="./%s">%s</a> -- SHA1 '%s'
        </li>
      </ul>
    </p>\n''' % (release_name, archive_name, archive_name, SHA)
  
  return replace_tag_on_the_site(fn, d_to_find, info_to_add)


def change_main_file_name(filename, old_name, new_name):
  """changes the references to the dynamically changed name"""
    
  return replace_tag_on_the_site(filename, u"<a href=\"%s\">" % old_name, u"<a href=\"%s\">" % new_name, True)

def insert_menu(filename):
  """changes the references to the dynamically changed name"""
  print "adding menu to", filename
  return replace_tag_on_the_site(filename, u"<!-->automated menu part<-->", do_menu(filename), True)

def save_file(content, filename):
  f = file(filename, "w")
  f.writelines(content)
  f.close()
  return

def do_the_website(archive, sha, release_name, release_date):
  """Simple function for updating the website"""
  file_name_html = os.path.join(CC.YAYI_TEMP_DIR, release_date, website_file[1])
  u = add_new_archive_to_the_site(os.path.split(archive)[1], sha, release_name, website_file[0])
  save_file(u, file_name_html)
  
  sites = other_web_site_files
  sites.extend([file_name_html])
  
  out = []
  for fn in sites:
    file_to_save = os.path.join(CC.YAYI_TEMP_DIR, release_date, os.path.split(fn)[1])

    # menu
    u = insert_menu(fn)    
    save_file(u, file_to_save + '.tmp1')
    
    # fichier principal
    u = change_main_file_name(file_to_save + '.tmp1', os.path.split(website_file[0])[1], os.path.split(file_name_html)[1])
    save_file(u, file_to_save)

    # suppression du fichier temporaire
    os.remove(file_to_save + '.tmp1')

    out.append(file_to_save)
  
  u = raw_input('Store the new website to the SVN Tree ? [y/n]')
  if(u == 'y'):
    u = add_new_archive_to_the_site(os.path.split(archive)[1], sha, release_name, website_file[0])
    save_file(u, file_name_html + '.temporary')  
    shutil.copyfile(file_name_html + '.temporary', website_file[0])
  
  return out


if __name__ == '__main__':
  do_the_website("blablabla", "lkjhqdlksjhqksdjh", "truc", "release_2009_12_01")

