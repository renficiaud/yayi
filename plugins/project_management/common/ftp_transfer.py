#!/usr/bin/env python
# -*- coding: utf_8 -*-
# (c) 2015 Raffi Enficiaud


"""
This is a small script that allows the transfer of a full directory via FTP. A possible 
way of running it would be

.. code::

  python -m common.ftp_transfer --url=ftp.yayimorphology.org --username=user --password=pass \
    --root_folder=~/dev/my_local_folder/ --destination=remote_folder

"""

import os
import sys
import time
import shutil
import ftplib

import logging

# logging facility
FORMAT = '[%(asctime)-15s] %(message)s'
logging.basicConfig(format=FORMAT)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

from .common import get_files


class FtpPathCache():
    """Manages remote paths and caches them"""

    def __init__(self, ftp_h):
        """
        :param ftp_h: existing ftp connection (ftplib.FTP object)
        """
         
        self.ftp_h = ftp_h
        self.path_cache = []

    def ftp_exists(self, path):
        '''path exists check function for ftp handler'''
        exists = None
        if path not in self.path_cache:
            try:
                self.ftp_h.cwd(path)
                exists = True
                self.path_cache.append(path)
            except ftplib.error_perm, e:
                if str(e.args).count('550'):    
                    exists = False
        else:
            exists = True

        return exists


    def ftp_mkdirs(self, path, sep='/'):
        '''mkdirs function for ftp handler'''
        split_path = path.split(sep)

        new_dir = ''
        for server_dir in split_path:
            if server_dir:
                new_dir += sep + server_dir
                if not self.ftp_exists(new_dir):
                    try:
                        logger.info('Create directory %s ...', new_dir)
                        self.ftp_h.mkd(new_dir)
                    except Exception, e:
                        logger.error('ERROR Creating directory %s: %s -- %s', new_dir, str(e.args))                




def transfer_files(list_files, ftp_site, root_folder, destination_folder):
    """Transfer a folder by FTP to the remote server
    
    :param list_files: list of files: every path should be absolute
    :param ftp_site: dictionary containing the server url, login and password
    :param root_folder: root folder of the local files
    :param destination_folder: destination on the remote site
    
    """
  
    pass_ = None
    if((not ftp_site.has_key('pass')) or (ftp_site['pass'] is None)):
        raise Exception('the password should be provided')

    pass_ = ftp_site['pass']
  
    ftp = None
  
    try:
        ftp = ftplib.FTP(ftp_site['site'], ftp_site['user'], pass_)
    except Exception, e:
        print 'An exception occurred during the connection to %s@%s : verify the connection parameters' % (ftp_site['user'], ftp_site['site'])
        raise e
  
    
    root_folder = os.path.abspath(root_folder) if root_folder is not None else None
    
    # first slash is important
    destination_folder = '/' + destination_folder.rstrip('\\').strip('/') + '/' if destination_folder is not None else ""
    
    ftp_cache = FtpPathCache(ftp)
    if not ftp_cache.ftp_exists(destination_folder):
        ftp_cache.ftp_mkdirs(destination_folder)
        
    current_remote = ftp.pwd()
    
    for f in list_files:

        # local path
        f = os.path.abspath(f)
        relative_path_to_root = os.path.relpath(os.path.dirname(f), root_folder) if root_folder is not None else f
        
        print 'sending file', os.path.join(relative_path_to_root, os.path.basename(f))

        # remote directory
        remote_path = os.path.join(destination_folder, relative_path_to_root if relative_path_to_root != '.' else "")

        if not ftp_cache.ftp_exists(remote_path):
            ftp_cache.ftp_mkdirs(remote_path)

        # Change the directory if needed
        if current_remote != remote_path:
            try:       
                ftp.cwd('/' + remote_path.rstrip('/'))
                current_remote = remote_path
                continue_on = True
            except ftplib.error_perm, e:
                logger.error('[ftp] CWD ERROR on path %s -- %s', ftp.pwd(), str(e.args))
                ftp.retrlines('LIST')
                continue

        # certainly folders should be created as well        
        ftp_command = 'STOR %s' % os.path.basename(f)
        logger.debug('[ftp] command %s', ftp_command)
        ftp.storbinary(ftp_command, 
                       open(f, 'rb'))
  
        
        #break
    ftp.retrlines('LIST')
    ftp.quit()
    
    return


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Simple FTP transfer with folder supports')
    parser.add_argument('--root_folder', 
                        default=None,  
                        help='path to the folder containing all the files')

    parser.add_argument('--destination_folder', 
                        default=None,  
                        help='path to the remote destination folder')

    parser.add_argument('--username', 
                        default=None,  
                        help='username for the remote FTP')
    
    parser.add_argument('--password', 
                        default=None,  
                        help='password for the remote FTP')

    parser.add_argument('--url', 
                        default=None,  
                        help='url of the remote FTP')
    
    args = parser.parse_args()

    if args.root_folder is None:
        sys.exit("you have not specified the path of input files.")
    
    if args.url is None:
        sys.exit("you have not specified the url.")
    
    ftp_site = {'pass': args.password,
                'user': args.username,
                'site': args.url}
    
    args.root_folder = os.path.expanduser(args.root_folder)
    list_of_files = get_files(args.root_folder)
    
    if  args.destination_folder is not None \
        and len(args.destination_folder) > 0 \
        and args.destination_folder[0] != '/':
         args.destination_folder = '/' + args.destination_folder

    
    def remote_files(l, filename):
        return [i for i in l if os.path.basename(i).lower() != filename]
    def remote_folders(l, filename):
        return [i for i in l if i.find(filename) == -1]
    
    logger.info('# files to transfer before filtering: %d', len(list_of_files))
    # get rid of some local files
    list_of_files = remote_files(list_of_files, '.ds_store')
    list_of_files = remote_folders(list_of_files, '/.git/')
    
    logger.info('# files to transfer after filtering: %d', len(list_of_files)) 
    
    transfer_files(list_of_files, ftp_site, args.root_folder, args.destination_folder)
    
if __name__ == '__main__':
    main()