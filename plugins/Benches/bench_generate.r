# script to generate the plots associated to the benches


args <- commandArgs(TRUE)

bench_file 					= args[1]
bench_description 	= args[2]
bench_revision 			= args[3]
bench_date 					= args[4]
bench_nb_processors = args[5]



# decalage
radius_dec0_with_gradient = read.table(file.path(path_args, "fitting_channel_0.txt"))
radius_dec2_with_gradient = read.table(file.path(path_args, "fitting_channel_2.txt"))

# polynomes
polynomes = read.table(file.path(path_args, "fitting_polynoms.txt"))
print(polynomes)


polynomes_previous = NULL
if(file.exists(file.path(path_args, "fitting_polynoms_previous.txt")))
{
  polynomes_previous = read.table(file.path(path_args, "fitting_polynoms_previous.txt"))
  print("Previous polynomials")
  print(polynomes_previous)
}


radius_dec0 = radius_dec0_with_gradient[,1:2]
radius_dec2 = radius_dec2_with_gradient[,1:2]

# get the size of the images
if(file.exists(file.path(path_args, "fitting_image_sizes.txt")))
{
  image_sizes = read.table(file.path(path_args, "fitting_image_sizes.txt"))
  print("image_sizes")
  print(image_sizes)
  x_red = c(0, sqrt((image_sizes[,1]/2)^2 + (image_sizes[,2]/2)^2))
  x_blue= x_red
} else
{
  x_red = c(min(radius_dec0[,1]), max(radius_dec0[,1]))
  x_blue = c(min(radius_dec2[,1]), max(radius_dec2[,1]))
}
