Yayi
====

Yayi is a open-source image processing framework, with an extended support of Mathematical Morphology operators. 
It is released under the very permissive Boost license. 

The core of Yayi is entirely written in C++, mainly using templatized code and metaprogramming, which enables
a high level of genericity. It implements some of the main concepts
of Mathematical Morphology into an efficient and proven design. 