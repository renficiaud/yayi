#include "main.hpp"
#include <yayiImageCore/include/yayiImageCore_Impl.hpp>
#include <yayiMeasurements/measurements_min_max.hpp>
#include <yayiMeasurements/include/measurements_min_max_t.hpp>
#include <yayiPixelProcessing/include/image_constant_T.hpp>

BOOST_AUTO_TEST_SUITE(min_max)

BOOST_AUTO_TEST_CASE(min_max_simple)
{
  using namespace yayi;

  typedef Image<yaUINT8> image_type;

  image_type im_in;
  image_type* p_im[] = {&im_in};
  image_type::coordinate_type coord;
  coord[0] = 4;
  coord[1] = 3;

  for(unsigned int i = 0; i < sizeof(p_im) / sizeof(p_im[0]); i++) {
    BOOST_REQUIRE_EQUAL(p_im[i]->SetSize(coord), yaRC_ok);
    BOOST_REQUIRE_EQUAL(p_im[i]->AllocateImage(), yaRC_ok);
  }

  // input image
  {
    static const std::string s =
      "1 2 5 2 "
      "1 1 3 2 "
      "1 3 10 2 "
    ;
    std::istringstream is(s);

    BOOST_CHECK_MESSAGE(is >> im_in, "Error during the input streaming of the image");
  }

  variant out;
  BOOST_TEST(measurements::image_meas_min_max_t(im_in, out) == yaRC_ok);
  std::pair<image_type::pixel_type, image_type::pixel_type> v = out;

  BOOST_TEST(v.first == 1, "Bad output for min: " << v.first << " (result) != 1 (test)");
  BOOST_TEST(v.second == 10, "Bad output for max: " << v.second << " (result) != 10 (test)");

}


BOOST_AUTO_TEST_CASE(min_max_multivalued)
{
  using namespace yayi;
  typedef Image<pixelFs_3> image_type;
  image_type im_in;

  BOOST_TEST_REQUIRE(im_in.SetSize(image_type::coordinate_type(c2D(4, 3))) == yaRC_ok);
  BOOST_TEST_REQUIRE(im_in.AllocateImage() == yaRC_ok);

  const pixelFs_3 p_value(3.4, 0.1, 2.5);
  BOOST_TEST_REQUIRE(constant_image_t(p_value, im_in) == yaRC_ok);


  variant out;
  BOOST_TEST_REQUIRE(measurements::image_meas_min_max_t(im_in, out) == yaRC_ok);
  std::vector< std::pair<image_type::pixel_type::value_type, image_type::pixel_type::value_type> > v = out;
  BOOST_TEST( v.size() == 3 );

  for(int i = 0; i < 3; i++) {
    BOOST_TEST_CONTEXT("Channel " << i) {
      BOOST_TEST(v[i].first == p_value[i]);
      BOOST_TEST(v[i].second == p_value[i]);
    }
  }
}

BOOST_AUTO_TEST_CASE(min_max_multivalued_reset)
{
  using namespace yayi;
  using namespace yayi::measurements;

  s_meas_min_max<pixelFs_3> operator_min_max;

  for(int i = 0; i < 3; i++) {
    BOOST_TEST_CONTEXT("Channel " << i) {
      BOOST_TEST(operator_min_max.result()[i].first == boost::numeric::bounds<pixelFs_3::value_type>::highest());
      BOOST_TEST(operator_min_max.result()[i].second == boost::numeric::bounds<pixelFs_3::value_type>::lowest());
    }
  }

  const pixelFs_3 p1_value(3.4f, 0.1f, 2.5f);
  const pixelFs_3 p2_value(2.4f, 0.2f, 2.7f);

  operator_min_max(p1_value);
  operator_min_max(p2_value);

  BOOST_TEST(operator_min_max.result()[0].first == 2.4f);
  BOOST_TEST(operator_min_max.result()[0].second == 3.4f);

  BOOST_TEST(operator_min_max.result()[1].first == 0.1f);
  BOOST_TEST(operator_min_max.result()[1].second == 0.2f);

  BOOST_TEST(operator_min_max.result()[2].first == 2.5f);
  BOOST_TEST(operator_min_max.result()[2].second == 2.7f);


  operator_min_max.clear_result();

  for(int i = 0; i < 3; i++) {
    BOOST_TEST_CONTEXT("Channel " << i) {
      BOOST_TEST(operator_min_max.result()[i].first == boost::numeric::bounds<pixelFs_3::value_type>::highest());
      BOOST_TEST(operator_min_max.result()[i].second == boost::numeric::bounds<pixelFs_3::value_type>::lowest());
    }
  }

}

BOOST_AUTO_TEST_SUITE_END()

