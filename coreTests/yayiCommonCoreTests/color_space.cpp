#include "main.hpp"
#include <yayiCommon/common_colorspace.hpp>

BOOST_AUTO_TEST_SUITE(color_space)

BOOST_AUTO_TEST_CASE(color_space_equality)
{
  using namespace yayi;
  yayi::color_space cs1(yayi::color_space::ecd_rgb), cs2(yayi::color_space::ecd_rgb);
  BOOST_CHECK_EQUAL(cs1, cs2);
  BOOST_CHECK_EQUAL(cs1, cs1);
  BOOST_CHECK_EQUAL(cs2, cs1);
  
  yayi::color_space cs3(yayi::color_space::ecd_rgb, yayi::color_space::ei_d65);
  BOOST_CHECK_NE(cs1, cs3);
  BOOST_CHECK_NE(cs3, cs1);
  BOOST_CHECK_EQUAL(cs3, cs3);
}

BOOST_AUTO_TEST_SUITE_END()


