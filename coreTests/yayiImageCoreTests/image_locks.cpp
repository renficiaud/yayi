#include "main.hpp"
#include <yayiImageCore/include/yayiImageCore_Impl.hpp>
#include <yayiImageCore/include/yayiImageUtilities_T.hpp>

#include <iostream>

#include <unordered_set>
#include <mutex>

namespace yayi
{

  namespace details_core_image
  {

    template <int dim>
    struct opaque_image_locks;

  
    /*! Used for RAII on rectangle locks
     */
    template <int dim>
    struct hyperrectangle_locker
    {
      opaque_image_locks<dim> &lock_manager;
      s_hyper_rectangle<dim> rect;
      bool is_write_lock;
      bool is_valid;

      /*! Constructor
       *
       * @param lock_manager_ reference to the lock manager
       * @param rect_ rectangle on which the lock is operating
       * @param is_write_lock_ is the lock held on a write area
       * @param is_valid_ is the lock valid
       *
       */
      hyperrectangle_locker(
        opaque_image_locks<dim> &lock_manager_,
        s_hyper_rectangle<dim> const &rect_,
        bool is_write_lock_,
        bool is_valid_) :
        lock_manager(lock_manager_),
        rect(rect_),
        is_write_lock(is_write_lock_),
        is_valid(is_valid_)
      {}

      //! Move constructor
      //! @post
      //! Right is not valid anymore.
      hyperrectangle_locker(hyperrectangle_locker&& right) noexcept :
        lock_manager(right.lock_manager),
        rect(right.rect),
        is_write_lock(right.is_write_lock),
        is_valid(right.is_valid)
      {
        right.is_valid = false;
      }
    
      ~hyperrectangle_locker()
      {
        release_lock();
      }

      //! Explicitely release the lock
      void release_lock()
      {
        if(is_valid)
        {
          lock_manager.release_lock(rect);
        }
        is_valid = false;
      }

      //! Returns true if the lock is active
      bool is_active() const
      {
        return is_valid;
      }
    };
  
    /*! Locks manager
     * 
     * Can manage read or write locks on rectangles over the image. Read locked rectangles
     * may overlap, but write lock rectangles are exclusive and should not overlap with any
     * other locked rectangle (either read or write).
     */
    template <int dim>
    struct opaque_image_locks
    {
      using hyperrectangle = s_hyper_rectangle<dim>;
      
      typedef hyperrectangle_locker<dim> hlocker_t;
      std::unordered_set<hyperrectangle> locked_read_rectangles;
      std::unordered_set<hyperrectangle> locked_write_rectangles;
      
      typedef std::recursive_mutex mutex_t;
      typedef std::unique_lock<mutex_t> lock_t;

      mutable mutex_t mutex_;

      //! Returns true if there is any active lock
      bool is_locked() const
      {
        return locked_write_rectangles.size() > 0 || locked_read_rectangles.size() > 0;
      }
      
      //! Returns true if there is a read or write lock to the specified region.
      bool is_locked(const hyperrectangle& candidate) const
      {
        lock_t l(mutex_);
        for(auto container : {&locked_write_rectangles, &locked_read_rectangles})
        {
          for(auto&& r : *container)
          {
            if(r.intersects(candidate))
              return true;
          }
        }
        return false;
      }
      
      hlocker_t acquire_lock(const hyperrectangle& candidate, bool is_write_lock)
      {
        // check for invalid rectangles
        if(candidate.Size() < s_coordinate<dim>(1))
        {
          return hlocker_t(*this, hyperrectangle(), false, false);
        }

        if(candidate.Origin() < s_coordinate<dim>(0))
        {
          return hlocker_t(*this, hyperrectangle(), false, false);
        }

        lock_t l(mutex_);
        if(is_write_lock)
        {
          if(!is_locked(candidate))
          {
            return hlocker_t(*this, hyperrectangle(), false, false);
          }
          else
          {
            locked_write_rectangles.insert(candidate);
          }
        }
        else
        {
          locked_read_rectangles.insert(candidate);
        }

        return hlocker_t(*this, candidate, is_write_lock, true);

      }
      
      void release_lock(const hyperrectangle& rect)
      {
        lock_t l(mutex_);
        locked_read_rectangles.erase(rect);
        locked_write_rectangles.erase(rect);
      }
    };


  }



  template <class T, int dim>
  struct locked_image : Image<T, s_coordinate<dim> >
  {
    void *opaque_image_locks_p;
    using rectangle_lock = typename details_core_image::opaque_image_locks<dim>::hlocker_t;

    using hyperrectangle = s_hyper_rectangle<dim>;
    
    locked_image();
    virtual ~locked_image();

    /*! Acquires a lock on the specified area
     *
     * @param rect the area
     * @param is_write_lock if true, the lock is exclusive (no other lock existing on this area)
     * @return if the area is not valid or if the area is already locked while requesting write access, 
     *         a lock holder that is invalid. Otherwise the lock holder of this area.
     *
     */
    rectangle_lock acquire_lock(hyperrectangle const& rect, bool is_write_lock);

    //! Returns true if there is any active lock on the image.
    bool is_locked() const;
    
  };
  
  template <class T, int dim>
  locked_image<T, dim>::locked_image() : opaque_image_locks_p(nullptr)
  {
    opaque_image_locks_p = new details_core_image::opaque_image_locks<dim>();
  }

  template <class T, int dim>
  locked_image<T, dim>::~locked_image()
  {
    details_core_image::opaque_image_locks<dim>* p = static_cast<details_core_image::opaque_image_locks<dim>*>(opaque_image_locks_p);
    delete p;
    opaque_image_locks_p = nullptr;
  }

  template <class T, int dim>
  typename locked_image<T, dim>::rectangle_lock
  locked_image<T, dim>::acquire_lock(hyperrectangle const& rect, bool is_write_lock)
  {
    details_core_image::opaque_image_locks<dim>* p = static_cast<details_core_image::opaque_image_locks<dim>*>(opaque_image_locks_p);
    return p->acquire_lock(rect, is_write_lock);
  }

  template <class T, int dim>
  bool
  locked_image<T, dim>::is_locked() const
  {
    details_core_image::opaque_image_locks<dim>* p = static_cast<details_core_image::opaque_image_locks<dim>*>(opaque_image_locks_p);
    return p->is_locked();
  }
}

BOOST_AUTO_TEST_CASE(image_lock_basic)
{
  using namespace yayi;
  using image_t = locked_image<yaUINT8, 4>;
  image_t im;
  im.SetSize(image_t::coordinate_type({10, 10, 10, 10}));

  {
    BOOST_CHECK(!im.is_locked());
  }

  {
    image_t::rectangle_lock rlock = std::move(im.acquire_lock({{0,0,0,0}, {2,2,2,2}}, false));
    BOOST_CHECK(im.is_locked());
  }

  {
    BOOST_CHECK(!im.is_locked());
  }

}

BOOST_AUTO_TEST_CASE(image_lock_write_exclusive)
{
  using namespace yayi;
  using image_t = locked_image<yaUINT8, 4>;
  image_t im;
  im.SetSize(image_t::coordinate_type({10, 10, 10, 10}));

  {
    BOOST_CHECK(!im.is_locked());
  }

  {
    image_t::rectangle_lock rlock = std::move(im.acquire_lock({{0,0,0,0}, {2,2,2,2}}, false));
    BOOST_CHECK(im.is_locked());
  }

  {
    BOOST_CHECK(!im.is_locked());
  }

}
