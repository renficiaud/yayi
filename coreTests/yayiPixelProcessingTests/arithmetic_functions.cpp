#include "main.hpp"
#include <yayiImageCore/include/yayiImageCore_Impl.hpp>
#include <yayiPixelProcessing/include/image_arithmetics_t.hpp>
#include <yayiPixelProcessing/include/image_constant_T.hpp>

BOOST_AUTO_TEST_SUITE(arithmetic_tests)

BOOST_AUTO_TEST_CASE(aabssub_on_multivalued_images)
{
  using namespace yayi;
  typedef Image<pixelFs_3, s_coordinate<2> > image_2f_t;

  image_2f_t im2D_1, im2D_2, im2D_3;
  BOOST_TEST_REQUIRE(im2D_1.SetSize(c2D(5, 6)) == yaRC_ok);
  BOOST_TEST_REQUIRE(im2D_1.AllocateImage() == yaRC_ok);

  BOOST_TEST_REQUIRE(im2D_2.SetSize(c2D(5, 6)) == yaRC_ok);
  BOOST_TEST_REQUIRE(im2D_2.AllocateImage() == yaRC_ok);

  BOOST_TEST_REQUIRE(im2D_3.SetSize(c2D(5, 6)) == yaRC_ok);
  BOOST_TEST_REQUIRE(im2D_3.AllocateImage() == yaRC_ok);

  BOOST_TEST(constant_image_t(pixelFs_3(3.4f, 2.f, 1.1f), im2D_1) == yaRC_ok);
  BOOST_TEST(constant_image_t(pixelFs_3(6.4f, 1.f, 1.2f), im2D_2) == yaRC_ok);

  // first check
  BOOST_TEST(abssubtract_images_t(im2D_1, im2D_2, im2D_3) == yaRC_ok);

  for(image_2f_t::const_iterator it(im2D_3.begin_block()), ite(im2D_3.end_block());
      it < ite;
      ++it)
  {
    BOOST_TEST( (*it)[0] == 3.f );
    BOOST_TEST( (*it)[1] == 1.f );
    BOOST_TEST( (*it)[2] == 0.1f, boost::test_tools::tolerance(0.001f) );
  }

  // checking commutativity
  BOOST_TEST(abssubtract_images_t(im2D_2, im2D_1, im2D_3) == yaRC_ok);

  for(image_2f_t::const_iterator it(im2D_3.begin_block()), ite(im2D_3.end_block());
      it < ite;
      ++it)
  {
    BOOST_TEST( (*it)[0] == 3.f );
    BOOST_TEST( (*it)[1] == 1.f );
    BOOST_TEST( (*it)[2] == 0.1f, boost::test_tools::tolerance(0.001f) );
  }

  // checking zeros
  BOOST_TEST(abssubtract_images_t(im2D_1, im2D_1, im2D_3) == yaRC_ok);

  for(image_2f_t::const_iterator it(im2D_3.begin_block()), ite(im2D_3.end_block());
      it < ite;
      ++it)
  {
    BOOST_TEST( (*it)[0] == 0 );
    BOOST_TEST( (*it)[1] == 0 );
    BOOST_TEST( (*it)[2] == 0 );
  }


}

BOOST_AUTO_TEST_SUITE_END()
