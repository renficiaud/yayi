#ifndef YAYI_IO_TESTS_MAIN_HPP__
#define YAYI_IO_TESTS_MAIN_HPP__

#include <boost/test/unit_test_suite.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/test_tools.hpp>
using boost::unit_test::test_suite;

#include <iostream>

#endif 

