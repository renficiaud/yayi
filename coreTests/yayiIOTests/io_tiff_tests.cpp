
#include "main.hpp"
#include <boost/test/parameterized_test.hpp>
#include <yayiIO/include/yayi_IO.hpp>
#include <yayiImageCore/include/yayiImageCore_Impl.hpp>
#include <yayiPixelProcessing/image_arithmetics.hpp>
#include <yayiMeasurements/measurements_min_max.hpp>
#include <yayiCommonCoreTests/data_path_for_tests.hpp>


BOOST_AUTO_TEST_SUITE(tiff_io)

void ReadTIFFTest(
  const std::string& filename,
  yayi::type image_type,
  bool should_fail = false,
  yayi::color_space cs = yayi::color_space(),
  const std::string &ground_truth_image_in_raw_format = "")
{
  using namespace yayi;

  IImage *im_tmp = nullptr;

  std::string im_file_name(get_data_from_data_path("libtiff/" + filename));
  yaRC ret = yayi::IO::readTIFF(im_file_name, 0, im_tmp);
  std::unique_ptr<IImage> im(im_tmp);

  if(!should_fail)
  {
    BOOST_TEST_REQUIRE(ret == yaRC_ok, "read error: " + static_cast<string_type>(ret) + " / file: " + filename);
    BOOST_TEST_REQUIRE(im.get() != (void*)nullptr);
    BOOST_TEST_REQUIRE(im->IsAllocated());
    BOOST_TEST(
      im->DynamicType() == image_type,
      "image of the wrong type image:" << im->DynamicType() << " / should be: " << image_type);

    BOOST_TEST(yayi::IO::writeRAW(std::string("./test_") + filename + ".raw", im.get()) == yaRC_ok);

    std::cout << "Image " << filename << " has color space \"" << im->GetColorSpace() << "\"" << std::endl;
    if(cs != yayi::color_space()) {
      BOOST_TEST(im->GetColorSpace() == cs);
    }

    if(!ground_truth_image_in_raw_format.empty()) {
      IImage *im_tmp2 = nullptr;
      std::string gt_file_name(get_data_from_data_path("libtiff/" + ground_truth_image_in_raw_format));
      yaRC ret = yayi::IO::readRAW(gt_file_name, im->GetSize(), image_type, im_tmp2);
      std::unique_ptr<IImage> gt_im(im_tmp2);

      BOOST_TEST_REQUIRE(ret == yaRC_ok, "read error: " + static_cast<string_type>(ret) + " / file: " + gt_file_name);
      BOOST_TEST_REQUIRE(image_abssubtract(im_tmp, im_tmp2, im_tmp2) == yaRC_ok);

      variant min_max;
      BOOST_TEST_REQUIRE(measurements::image_meas_min_max(im_tmp2, min_max) == yaRC_ok);


      if(image_type.c_type == yayi::type::c_scalar)
      {
          std::pair<double, double> out = min_max;
          BOOST_TEST_REQUIRE(out.first == 0);
          BOOST_TEST_REQUIRE(out.second == 0);
      }
      else
      {
          std::vector< std::pair<double, double> > v_out = min_max;
          BOOST_TEST_REQUIRE(v_out.size() >= 3);
          for(int i = 0; i < v_out.size(); i++) {
            BOOST_TEST_CONTEXT("Channel " << i) {
              BOOST_TEST(v_out[i].first == 0);
              BOOST_TEST(v_out[i].second <= 1);
            }
          }
      }
    }

  }
  else
  {
    BOOST_TEST(ret != yaRC_ok, "read error (should fail): " + static_cast<string_type>(ret) + " / file: " + filename );
    BOOST_TEST_REQUIRE(im.get() == (void*)nullptr, "null image (should fail)");
  }

/*
  IImage::coordinate_type coord = im->GetSize();
  //BOOST_CHECK_MESSAGE(ret == yaRC_ok, "cannot get the image size " + static_cast<string_type>(ret));
  BOOST_CHECK_MESSAGE(coord.dimension() == 2, "bad dimension");
  BOOST_CHECK_MESSAGE(coord[0] == 10, "bad dimension");
  BOOST_CHECK_MESSAGE(coord[1] == 20, "bad dimension");

  typedef Image<yaUINT8> image_type;
  image_type *im_t = dynamic_cast<image_type*>(im);


  BOOST_CHECK_MESSAGE(im_t != 0, "cast error");

  for(int i = 0; i < coord[0] * coord[1]; i++) {
    BOOST_CHECK_MESSAGE(im_t->pixel(i) == i, "pixel bad value");
  }

*/
}

BOOST_AUTO_TEST_CASE(minisblack_1c_8b)
{
  ReadTIFFTest("minisblack-1c-8b.tiff",
               yayi::to_type<yayi::yaUINT8>());
}

BOOST_AUTO_TEST_CASE(minisblack_1c_16b)
{
  ReadTIFFTest("minisblack-1c-16b.tiff",
               yayi::to_type<yayi::yaUINT16>());
}

BOOST_AUTO_TEST_CASE(rgb_3c_8b)
{
  ReadTIFFTest(
    "rgb-3c-8b.tiff",
    yayi::to_type<yayi::pixel8u_3>(),
    false,
    yayi::cs_sRGB,
    "gt_rgb-3c-8b.raw");
}

BOOST_AUTO_TEST_CASE(quad_tile)
{
  ReadTIFFTest(
    "quad-tile.jpg.tiff",
    yayi::to_type<yayi::pixel8u_3>(),
    false,
    yayi::cs_YCbCr601,
    "gt_test_quad-tile.jpg.raw");
}

BOOST_AUTO_TEST_CASE(tiled)
{
  ReadTIFFTest(
    "release-grosse bouche_tiled.tiff",
    yayi::to_type<yayi::pixel8u_3>(),
    false,
    yayi::cs_sRGB,
    "gt_test_release-grosse bouche_tiled.raw");
}

BOOST_AUTO_TEST_CASE(planar_big_endian_zip)
{
  ReadTIFFTest(
    "release-grosse bouche_planar_big_endian_zip.tiff",
    yayi::to_type<yayi::pixel8u_3>(),
    false,
    yayi::cs_sRGB,
    "gt_test_release-grosse bouche_tiled.raw"); // same content as the previous image
}

BOOST_AUTO_TEST_CASE(interleaved_big_endian_zip_cmyk)
{
  ReadTIFFTest(
    "release-grosse bouche_interleaved_big_endian_zip_cmyk.tiff",
    yayi::to_type<yayi::pixel8u_4>());
}

BOOST_AUTO_TEST_CASE(interleaved_big_endian_zip_cmyk16bits)
{
  ReadTIFFTest(
    "release-grosse bouche_interleaved_big_endian_zip_cmyk16bits.tiff",
    yayi::to_type<yayi::pixel16u_4>());
}

BOOST_AUTO_TEST_CASE(interleaved_big_endian_zip_rgbFloat32bits)
{
  ReadTIFFTest(
    "release-grosse bouche_interleaved_big_endian_zip_rgbFloat32bits.tiff",
    yayi::to_type<yayi::pixelFs_3>());
}

BOOST_AUTO_TEST_CASE(interleaved_big_endian_zip_rgbFloat16bits)
{
  ReadTIFFTest(
    "release-grosse bouche_interleaved_big_endian_zip_rgbFloat16bits.tiff",
    yayi::to_type<yayi::pixelFs_3>(),
    true); // should fail, unsupported image type
}



namespace
{
  template <class P>
  void addForTest(P& p, const int i)
  {
    p += i;
  }

  template <class T, class I>
  void addForTest(yayi::s_compound_pixel_t<T, I>& p, const int i)
  {
    p[0] += i;
  }

  template <class T>
  T const & to_string(T const &t){return t;}

  inline int to_string(char const &t){return t;}
  inline int to_string(unsigned char const &t){return t;}
}

typedef boost::mpl::list<
  yayi::yaUINT8, yayi::yaUINT16, yayi::yaUINT32, yayi::yaUINT64,
  yayi::yaINT8, yayi::yaINT16, yayi::yaINT32, yayi::yaINT64 ,
  yayi::yaF_simple, yayi::yaF_double,

  yayi::pixel8u_3, yayi::pixel16u_3,
  yayi::pixel8u_4, yayi::pixel16u_4,

  yayi::pixelFs_3, yayi::pixelFd_3
  > test_types;

BOOST_AUTO_TEST_CASE_TEMPLATE(WriteTIFFTest, pixel_type, test_types)
{
  using namespace yayi;

  typedef Image<pixel_type> image_t;
  image_t im;
  im.SetSize(c2D(1000, 1000));
  BOOST_REQUIRE_EQUAL(im.AllocateImage(), yaRC_ok);

  pixel_type p = 0;
  for(typename image_t::iterator it(im.begin_block()), ite(im.end_block()); it != ite; ++it, addForTest(p, 1)/*+= 1*/)
  {
    *it = p;
  }

  BOOST_CHECK_EQUAL(yayi::IO::writeTIFF("test_im.tiff", &im), yaRC_ok);


  IImage *im2 = 0;

  BOOST_REQUIRE_EQUAL(yayi::IO::readTIFF("test_im.tiff", 0, im2), yaRC_ok);

  std::unique_ptr<IImage> p_im2(im2);
  BOOST_REQUIRE_NE(im2, (IImage *)0);

  image_t *im2t = dynamic_cast<image_t*>(im2);
  BOOST_REQUIRE_NE(im2t, (image_t*)0);

  p = 0;
  for(typename image_t::iterator it(im2t->begin_block()), ite(im2t->end_block()); it != ite; ++it, addForTest(p, 1)/*p+= 1*/)
  {
    BOOST_CHECK_MESSAGE(*it == p, "error at position " << it.Position() << "*it = " << to_string(*it) << " != p = " << to_string(p));
  }

}

BOOST_AUTO_TEST_SUITE_END()
