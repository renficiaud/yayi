Development guidelines {#devel}
======================

[TOC]

C++ {#cpp}
===

* the C++ code uses lower case with underscores

Documentation {#doc}
============= 

* the C++ documentation uses [Doxygen](http://www.stack.nl/~dimitri/doxygen).
* the Python documentation uses [Sphinx](http://sphinx-doc.org/)

Doxygen
-------
* The main documentation pages were recently translated to Markdown, which is more convenient for writing. 
* It would be nice to be able to keep the references of the articles for the algorithm implementations. Doxygen 
  allows to do that with the \@cite command, that refers to a bib entry. The bibtex the project is using is
  in the repository.


How to contribute {#contribute}
=================

Any kind of contribution is more than welcome! The easiest way is to fork the library and then to submit pull requests. 
