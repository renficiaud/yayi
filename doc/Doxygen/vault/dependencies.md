Numpy {#install_numpy}
=====

[Numpy](http://numpy.scipy.org) is a Python package delivering highly optimised mathematical operations on array, 
matrices, etc. Yayi is able to import/export images from/to Numpy N dimensional array format, which then can be processed by Numpy or any Python 
package expecting a Numpy array. 

Installing Numpy is easy, except on MacOSX:
- On Ubuntu: this is quite easily done with the package manager. 
- On Windows: download the self-runnable package corresponding to your Python installation (from the Numpy website) and run it.
- On Mac OSX: to the author knowledge, the Numpy package is available only for official Python installation (from 
http://www.python.org), 
and not for the flavour of Python coming with Mac OSX. Once the official Python distribution installed, installing Numpy is straightfoward.

See @ref configure_numpy for enabling Numpy during the build of Yayi.


HDF5 {#install_hdf5}
====

[HDF5](http://www.hdfgroup.org/HDF5/) is a portable and extensible file format for storing data, among which multidimensional array (images). 
@code{bash}
$> cd your_path_to_uncompressed_hdf5_library
$> ./configure --prefix=$HDF5_INSTALL_DIR
$> make
$> make install
$> make check-install
@endcode

See @ref configure_HDF5 for enabling HDF5 during the build of Yayi.

