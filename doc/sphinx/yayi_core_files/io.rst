IO
==
The IO module provides image file input and output from disk. The following formats are supported:

* Jpeg
* extended support for TIFF file format (8/16/32 bits images, tiled or not)
* extented support for the PNG (8/16 bits images, 1/3/4 channels)
* a `generic` RAW image format, that is just a RAW memory flush to disk
* it is able to save images with the Encapsulated Postscript (EPS) format
* if enabled at compilation time, yayi supports the HDF5 file format


Reference
---------

.. automodule:: YayiIOPython
    :members: