Pixel processing
================
This module contains pixel-to-pixel transformations of any kind:

* color space transformations
* image arithmetics
* logical image combinations


Reference
---------

.. automodule:: YayiPixelProcessingPython
    :members:
    :undoc-members: