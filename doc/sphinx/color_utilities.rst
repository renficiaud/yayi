color_utilities
===============
This file contains some useful wrappers over Yayi basic color processing functions.

.. contents:: Content
        :local:


.. automodule:: Yayi.color_utilities
    :members:
