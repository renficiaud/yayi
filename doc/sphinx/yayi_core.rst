Yayi core python extensions
***************************
This page details the python bindings of Yayi. 
Since these are bindings, for each class or function described here, there exist the equivalent C++ class or function. 
The python modules are also consistent with their C++ counterparts.


.. toctree::
   
   yayi_core_files/common.rst
   yayi_core_files/io.rst
   yayi_core_files/pixel_processing.rst

.. contents:: 





Reference
---------



Core
====
.. automodule:: Yayi.bin.YayiImageCorePython
    :members:
    

Structuring Elements
====================
.. automodule:: YayiStructuringElementPython
    :members:

Neighborhood Processing
=======================
.. automodule:: YayiNeighborhoodProcessingPython
    :members:

Low level morphology
====================
.. automodule:: YayiLowLevelMorphologyPython
    :members:
    
Labels
======
.. automodule:: YayiLabelPython
    :members:

Reconstructions
===============
.. automodule:: YayiReconstructionPython    
    :members:

Segmentation
============

This module contains segmentation methods. 

Example
-------

Reference
---------

.. automodule:: YayiSegmentationPython    
    :members:

Measurements
============
.. automodule:: YayiMeasurementsPython    
    :members:

Distances
=========
.. automodule:: YayiDistancesPython    
    :members:
