.. Yayi documentation master file, created by
   sphinx-quickstart on Mon May 21 22:20:57 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Yayi's documentation!
================================
This is the documentation of Yayi, a nice and powerful image processing/mathematical morphology library with python bindings.


.. contents:: This page contains

.. toctree::
  :maxdepth: 1
  


Introduction
============
Yayi is an image processing and mathematical morphology toolbox. It is mainly written in C++, but has Python bindings enabling its use from Python.
Yayi is available on all known platforms and uses technologies that are cross-platform and free.

How to get Yayi?
^^^^^^^^^^^^^^^^
Yayi depends on a few external libraries, which should be available prior to its intallation. Once installed, the only thing you need to do is::

    pip install yayi


How to install Yayi?
^^^^^^^^^^^^^^^^^^^^
.. toctree::
  :maxdepth: 2

  installing_yayi


Yayi python helper functions
============================
Mostly companion methods in order to ease the script and design of new methods.

Details
^^^^^^^
.. toctree::
  :maxdepth: 2

  helper_scripts


A complete example of use
=========================
.. toctree::
  :maxdepth: 2

  complete_example



Yayi python bindings
====================
Yayi python bindings are performed using boost.python.


.. toctree::
  :maxdepth: 2

  yayi_core


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

