draw
====
Drawing functions/utilities using Yayi.

.. automodule:: Yayi.draw
    :members:
