
morpho_utilities
================
Wrappers over morphological functions.

.. automodule:: Yayi.morpho_utilities
    :members:

