image_utilities
===============
This file contains some useful wrappers over Yayi basic image processing functions.

.. automodule:: Yayi.image_utilities
    :members:

