
set(INTERPROCEDURAL_OPTIMIZATION_RELEASE)

message(STATUS "[YAYI][COMPILER] host processor " ${CMAKE_HOST_SYSTEM_PROCESSOR} " target processor " ${CMAKE_SYSTEM_PROCESSOR})
option(USE_SSE2   "Enable SEE2 instructions,target processor must support it" ON)
option(USE_SSE4   "Enable SEE4 instructions,target processor must support it" ON)
option(USE_AVX    "Enable AVX instructions,target processor must support it"  OFF)
option(USE_AVX2   "Enable AVX2 instructions,target processor must support it" OFF)

if(MSVC)

  message(STATUS "[YAYI][COMPILER] MSVC compilation flags")

  # suppress these annoying message
  add_definitions(-D_SCL_SECURE_NO_WARNINGS)
  add_definitions(-D_CRT_SECURE_NO_DEPRECATE)
  add_definitions(-D_CRT_NONSTDC_NO_DEPRECATE)



  #TR i don't find a way to reject generation of project if ${MSVC_VERSION}"<1500
  #message(STATUS "MSVC_VERSION ${MSVC_VERSION}")
  #Visual C++, 32-bit, version 6.0         1200
  #Visual C++, 32-bit, version .net 2002   1300
  #Visual C++, 32-bit, version .net 2003   1310
  #Visual C++, 32-bit, version 2005        1400 (vc80)
  #Visual C++, 32-bit, version 2005 SP1    14?? (vc80_sp1)
  #Visual C++, 32-bit, version 2008        1500 (vc90)

  # Setup 64bit and 64bit windows systems
  if(CMAKE_CL_64)
    set(PLATFORM_LINKFLAGS "${PLATFORM_LINKFLAGS} /MACHINE:X64")
    # This definition is necessary to work around a bug with Intellisense described
    # here: http://tinyurl.com/2cb428.  Syntax highlighting is important for proper
    # debugger functionality.
    add_definitions("-D_WIN64")

    #Enable extended object support for debug compiles on X64 (not required on X86)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /bigobj")
  endif()

  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /RTC1") #/rtc1 full runtime check
  set(CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG} /RTC1")

  #/Ox full optim
  #/Ob (Inline Function Expansion), where the option parameter is 2 (/Ob2)
  #and  /Og (Global Optimizations)
  #and  /Oi (Generate Intrinsic Functions)
  #and /Ot (Favor Fast Code)
  #AND /Oy (Frame-Pointer Omission) TODO check on x64
  #/GF pools strings as read-only.

  #TODO remove some flag not compatible with 64 bits
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /Ox /Oi /Ob2 /Ot /Oy /GF /GL /D\"_SECURE_SCL=0\"")
  set(CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} /Ox /Oi /Ob2 /Oy /GF /GL")

  if(USE_AVX2)
    message(STATUS "[YAYI] Compiling 'release' with AVX2 support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /arch:AVX2")
    set(CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} /arch:AVX2")
  elseif(USE_AVX)
    message(STATUS "[YAYI] Compiling 'release' with AVX support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /arch:AVX")
    set(CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} /arch:AVX")
  elseif(USE_SSE2)
    message(STATUS "[YAYI] Compiling 'release' with SSE2 support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /arch:SSE2")
    set(CMAKE_C_FLAGS_RELEASE   "${CMAKE_C_FLAGS_RELEASE} /arch:SSE2")
  endif()

  set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /LTCG")

  # turn off various warnings
  #4250: 'class1' : inherits 'class2::member' via dominance  polymorphism generate 10^50 warning ....
  #4702 unreachable code
  #4715 not all control paths return a value
  foreach(warning 4250 4702 4745 )
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd${warning}")
  endforeach(warning)


  # This option is to enable the /DYNAMICBASE switch
  # It is used to workaround a bug in Windows 7 when linking in release, which results in corrupt
  # binaries. See this page for details: http://www.wintellect.com/CS/blogs/jrobbins/archive/2009/01/24/the-case-of-the-corrupt-pe-binaries.aspx
  OPTION(WIN32_USE_DYNAMICBASE "Set to ON to build with the /DYNAMICBASE option to work around a bug when linking release executables on Windows 7." ON)
  MARK_AS_ADVANCED(WIN32_USE_DYNAMICBASE)
  if(WIN32_USE_DYNAMICBASE)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /DYNAMICBASE")
  endif()

  #more than 3gb
  OPTION(WIN32_LARGEADRESSAWARE "Set to ON to build with large adress aware option (usefull on x64 systems)" ON)
  if(WIN32_LARGEADRESSAWARE)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /LARGEADDRESSAWARE")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} /LARGEADDRESSAWARE")
  endif()



  #This option is to enable the /MP switch for Visual Studio 2005 and above compilers
  OPTION(WIN32_USE_MP "Set to ON to build with the /MP option (Visual Studio 2005 and above)." OFF)
  MARK_AS_ADVANCED(WIN32_USE_MP)
  if(WIN32_USE_MP)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")
  endif()

  # Boost specific
  remove_definitions(-DBOOST_TEST_DYN_LINK)
  add_definitions(-DBOOST_ALL_NO_LIB)
  message(STATUS "[YAYI][COMPILER] Flags for MSVC: ${CMAKE_CXX_FLAGS}")


endif(MSVC)


if(NOT MSVC AND (("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU") OR ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")))
  include(CheckCXXCompilerFlag)
  check_cxx_compiler_flag("-std=c++14" COMPILER_SUPPORTS_CXX14)
  check_cxx_compiler_flag("-std=c++11" COMPILER_SUPPORTS_CXX11)
  if(COMPILER_SUPPORTS_CXX14)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
  elseif(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  else()
    message(FATAL_ERROR "[YAYI][COMPILER] the compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
  endif()

  if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
  endif()
endif()



if(CMAKE_COMPILER_IS_GNUCXX)
  message(STATUS "[YAYI][COMPILER] flags for GNUCXX")

  check_cxx_compiler_flag(-pthread HAS_MINUS_PTHREAD)

  # some problems with the amd 64bits and gcc
  if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64")
    set(CMAKE_C_FLAGS           "${CMAKE_C_FLAGS} -fPIC")
    set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -fPIC")
  endif()

  if(HAS_MINUS_PTHREAD AND NOT APPLE)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
  endif()

  if(USE_AVX2)
    message(STATUS "[YAYI] Compiling 'release' with AVX2 support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -mavx2")
  elseif(USE_AVX)
    message(STATUS "[YAYI] Compiling 'release' with AVX support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -mavx")
  elseif(USE_SEE4)
    message(STATUS "[YAYI] Compiling 'release' with SSE4 support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -msse4")
  elseif(USE_SEE2)
    message(STATUS "[YAYI] Compiling 'release' with SSE2 support")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -msse2")
  endif()

  set(CMAKE_CXX_FLAGS_RELEASE   "${CMAKE_CXX_FLAGS_RELEASE} -O3")
  set(CMAKE_C_FLAGS_RELEASE     "${CMAKE_C_FLAGS_RELEASE} -O3")

  if(APPLE)
    set(CMAKE_OSX_ARCHITECTURES "i386;x86_64")
  endif()

endif()
