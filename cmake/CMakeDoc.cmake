

# to avoid having some ../.. in the paths
get_filename_component(YAYI_root_dir_absolute ${YAYI_root_dir} ABSOLUTE)
set(YAYI_DOCUMENTATION_ROOT ${YAYI_root_dir_absolute}/doc)


# SPHINX
# Getting sphinx availability
execute_process(
  COMMAND ${PYTHON_EXECUTABLE} -c "try:\n\timport sphinx\n\tprint('TRUE')\nexcept ImportError:\n\tprint('FALSE')"
  OUTPUT_VARIABLE SPHINX_AVAILABLE
  ERROR_VARIABLE  SPHINX_ERROR
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# error in checking ? should not occur
if(NOT (SPHINX_ERROR STREQUAL ""))
  message(FATAL_ERROR "Error: something wrong with the sphinx detection script: ${SPHINX_ERROR}")
endif()

if(SPHINX_AVAILABLE STREQUAL "FALSE")
  message(STATUS "[YAYIDoc] Sphinx not available")
else()
  message(STATUS "[YAYIDoc] Sphinx available from python: adding sphinx target")

  if(UNIX)
    # adds YAYIPATH to make environment
    if(APPLE)
      set(VARRPATH "DYLD_LIBRARY_PATH")
    else()
      set(VARRPATH "LD_LIBRARY_PATH")
    endif()
    set(SPHINX_COMMAND make YAYIPATH=$<TARGET_FILE_DIR:YayiCommonPython> ${VARRPATH}=$ENV{${VARRPATH}}:${Boost_LIBRARY_DIRS})
  elseif(WIN32)

    # test how to do it under windows...
    set(SPHINX_COMMAND "set YAYIPATH=$<TARGET_FILE_DIR:YayiCommonPython>\nset PATH=%PATH%\;${Boost_LIBRARY_DIRS}\nmake.bat")
  endif()




  add_custom_target(Sphinx
    #${SPHINX_COMMAND} html
    sphinx-build -b html ${YAYI_DOCUMENTATION_ROOT}/sphinx ${YAYI_PYTHON_PACKAGE_LOCATION}/docs/build/html
    WORKING_DIRECTORY ${YAYI_PYTHON_PACKAGE_LOCATION}
    COMMENT "Generating sphinx documentation sphinx-build -b html ${YAYI_DOCUMENTATION_ROOT}/sphinx ${YAYI_PYTHON_PACKAGE_LOCATION}/docs/build/html"
    #VERBATIM
    # we want the binaries to be there also
    DEPENDS PythonPackageSetup
    SOURCES
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/conf.py
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/index.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/color_utilities.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/complete_example.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/draw.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/helper_scripts.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/image_utilities.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/installing_yayi.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/morpho_utilities.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/yayi_core.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/yayi_core_files/common.rst
      ${YAYI_DOCUMENTATION_ROOT}/sphinx/yayi_core_files/io.rst
    )
  set_property(TARGET Sphinx PROPERTY FOLDER "Documentation/")

endif()




# ################
# Doxygen
set(YAYI_DOXYGEN_PATH ${YAYI_DOCUMENTATION_ROOT}/Doxygen)
find_file(DOXYGEN_CONFIG_FILE Doxyfile ${YAYI_DOXYGEN_PATH})
mark_as_advanced(DOXYGEN_CONFIG_FILE)


find_package(Doxygen)
if(NOT DOXYGEN_FOUND)
  message("Doxygen not found")
elseif(DOXYGEN_CONFIG_FILE)
  message(STATUS "[YAYIDoc] Doxygen: configuring documentation with file ${DOXYGEN_CONFIG_FILE}")

  if(DOXYGEN_DOT_FOUND)
    message(STATUS "[YAYIDoc] Doxygen dot found at ${DOXYGEN_DOT_PATH}")
  else()
    message(STATUS "[YAYIDoc] Doxygen dot not found")
  endif()


  ###### GENERAL ######
  set(DOXY_OUTPUT_DIR           ${CMAKE_BINARY_DIR}/documentation/doxygen)
  set(DOXYGEN_CONFIG_GENERATED  ${DOXY_OUTPUT_DIR}/Doxyfile.generated)

  option(DOXYGEN_GENERATE_HTML  "Generates HTML documentation"  TRUE)
  mark_as_advanced(DOXYGEN_GENERATE_HTML)
  option(DOXYGEN_GENERATE_XML   "Generates xml documentation"   TRUE)
  mark_as_advanced(DOXYGEN_GENERATE_XML)
  option(DOXYGEN_GENERATE_LATEX "Generates LateX documentation" FALSE)
  mark_as_advanced(DOXYGEN_GENERATE_LATEX)

  # configuring some paths
  set(DOXY_STRIP_FROM_PATH      ${YAYI_root_dir_absolute})
  set(DOXY_STRIP_FROM_INC_PATH  ${YAYI_root_dir_absolute}/core)
  set(DOXY_HTML_HEADER       ${YAYI_DOXYGEN_PATH}/header_bootstrap.html)
  set(DOXY_HTML_FOOTER       ${YAYI_DOXYGEN_PATH}/footer_bootstrap.html)
  set(DOXY_HTML_STYLESHEET_EXT      ${YAYI_DOXYGEN_PATH}/customdoxygen.css)

  mark_as_advanced(DOXY_HTML_HEADER)
  mark_as_advanced(DOXY_HTML_FOOTER)
  mark_as_advanced(DOXY_HTML_STYLESHEET_EXT)


  ###### INPUT FILES ######
  get_property(YAYI_CORE_DIR GLOBAL PROPERTY YAYI_CORE_DIR)
  set(DOXY_INPUT_DIRS \"${YAYI_DOCUMENTATION_ROOT}/Doxygen\")
  foreach(_DIR ${YAYI_CORE_DIR})
    list(APPEND DOXY_INPUT_DIRS \"${_DIR}/\")
  endforeach(_DIR ${YAYI_CORE_DIR})


  set(DOXY_FILES_PATTERN *.h *.hpp *.dox *.md) #add dox for mainpage and others see http://entrenchant.blogspot.com/2009/09/doxygen-gotchas.html
  set(DOXY_IMAGE_PATH   ${YAYI_DOCUMENTATION_ROOT}/images)
  set(DOXY_EXAMPLE_PATH ${YAYI_root_dir}/coreTests)
  set(DOXY_INCLUDE_PATH \"${JPEG_LIB_SRC}\" \"${PNG_LIB_SRC}\" \"${ZLIB_SRC_DIR}\" \"${Boost_INCLUDE_DIRS}\")
  set(DOXY_CITE_BIB_FILE "${YAYI_DOCUMENTATION_ROOT}/Latex/bib_all_utf8.bib")

  file(GLOB doxygen_extra_files ${YAYI_DOXYGEN_PATH}/icons/*.* ${DOXY_IMAGE_PATH}/*.*)
  set(DOXY_EXTRA_FILES \"${YAYI_DOXYGEN_PATH}/doxy-boot.js\")
  foreach(f ${doxygen_extra_files})
    list(APPEND DOXY_EXTRA_FILES \"${f}\")
  endforeach()

  # translating the cmake lists into dox
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_FILES_PATTERN    "${DOXY_FILES_PATTERN}")
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_INPUT_DIRS       "${DOXY_INPUT_DIRS}")
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_IMAGE_PATH       "${DOXY_IMAGE_PATH}")
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_EXAMPLE_PATH     "${DOXY_EXAMPLE_PATH}")
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_INCLUDE_PATH     "${DOXY_INCLUDE_PATH}")
  string(REGEX REPLACE ";" " \\\\ \n" DOXY_EXTRA_FILES      "${DOXY_EXTRA_FILES}")


  #### DOT ####
  if(DOXYGEN_DOT_FOUND)
    set(DOXY_HAVE_DOT YES)
    set(DOT_BINARY_PATH ${DOXYGEN_DOT_PATH})
  else()
    set(DOXY_HAVE_DOT NO)
    set(DOT_BINARY_PATH "")
  endif()

   ###### WARNINGS ######
  if(CMAKE_BUILD_TOOL MATCHES (msdev|devenv))
    set(DOXY_WARN_FORMAT "\"$file($line) : $text \"")
  else()
    set(DOXY_WARN_FORMAT "\"$file:$line: $text \"")
  endif()

  message(STATUS "[YAYIDoc] Configuring file ${DOXYGEN_CONFIG_FILE} to ${DOXYGEN_CONFIG_GENERATED}")

  # translates the ON/OFF to proper YES/NO
  foreach(_v DOXYGEN_GENERATE_XML DOXYGEN_GENERATE_LATEX DOXYGEN_GENERATE_HTML)
    set(${_v}_TRANSLATED "NO")
    if(DOXYGEN_GENERATE_XML)
      set(${_v}_TRANSLATED "YES")
    endif()
  endforeach()
  set(DOXY_EXCLUDE_PATHS_TRANSLATED ${CMAKE_BINARY_DIR})
  file(TO_CMAKE_PATH ${DOXY_EXCLUDE_PATHS_TRANSLATED} ${DOXY_EXCLUDE_PATHS_TRANSLATED})
  file(RELATIVE_PATH DOXY_EXCLUDE_PATHS_TRANSLATED ${YAYI_root_dir} ${DOXY_EXCLUDE_PATHS_TRANSLATED})
  set(DOXY_EXCLUDE_PATHS_TRANSLATED "./${DOXY_EXCLUDE_PATHS_TRANSLATED}/*")
  configure_file(${DOXYGEN_CONFIG_FILE} ${DOXYGEN_CONFIG_GENERATED} @ONLY)

  file(GLOB doxygen_pages ${YAYI_DOXYGEN_PATH}/*.dox ${YAYI_DOXYGEN_PATH}/*.md)
  set(doxygen_layout_files
      ${DOXY_HTML_STYLESHEET_EXT}
      ${YAYI_DOXYGEN_PATH}/footer_bootstrap.html
      ${YAYI_DOXYGEN_PATH}/header_bootstrap.html
    )

  add_custom_target(Doxygen
    ${DOXYGEN_EXECUTABLE} ${DOXYGEN_CONFIG_GENERATED}
    COMMENT "Generating doxygen documentation"
    DEPENDS ${DOXYGEN_CONFIG_FILE} ${DOXYGEN_CONFIG_GENERATED}
    WORKING_DIRECTORY ${YAYI_root_dir}
    SOURCES
      ${YAYI_root_dir}/cmake/CMakeDoc.cmake
      ${YAYI_DOXYGEN_PATH}/Doxyfile
      ${DOXYGEN_CONFIG_GENERATED}
      ${doxygen_pages}
      ${doxygen_layout_files}
      )
  set_property(TARGET Doxygen PROPERTY FOLDER "Documentation/")
  source_group(Pages FILES ${doxygen_pages})
  source_group(Layout FILES ${doxygen_layout_files})

  install(
    DIRECTORY ${DOXY_OUTPUT_DIR}/html/
    DESTINATION ${YAYI_DOCUMENTATION_INSTALLATION_RELATIVE_PATH}
    COMPONENT API-DOC
    PATTERN "html/*")


endif()
