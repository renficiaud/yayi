#ifndef YAYI_COLORPROCESS_YUV_T_HPP__
#define YAYI_COLORPROCESS_YUV_T_HPP__


#include <yayiImageCore/include/ApplyToImage_unary_t.hpp>
#include <yayiCommon/common_constants.hpp>


/*!@file
 * This file contains the transformations to YUV according to the standard.
 * @author Raffi Enficiaud
 */

namespace yayi
{
  /*!
   * @addtogroup pp_color_details_grp
   *
   *@{
   */

  namespace impl
  {
    //! Base class for matrix pixel transformations
    template <class pixel_in_t, class pixel_out_t, class matrix_element_t = yaF_simple>
    struct s_matrix_pixel_transformation_base
    {
      using value_type = typename pixel_out_t::value_type;
      matrix_element_t matrix_transform[pixel_out_t::dimension::value][pixel_in_t::dimension::value];

      const matrix_element_t ** const get_matrix() const
      {
        return matrix_transform;
      }
    };
  }

  //! Simple tag indicating that the function object implements a matrix
  //! transformation of each pixels.
  //!
  //! Classes implementing this tag are expected to implement a get_matrix function
  //! object.
  template <
    class pixel_in_t,
    class pixel_out_t,
    class matrix_element_t = yaF_simple>
  struct s_matrix_pixel_transformation
    : public impl::s_matrix_pixel_transformation_base<pixel_in_t, pixel_out_t, matrix_element_t>
  {
    using base_t = impl::s_matrix_pixel_transformation_base<pixel_in_t, pixel_out_t, matrix_element_t>;
    using value_type = typename base_t::value_type;

    pixel_out_t operator()(pixel_in_t const& u) const noexcept
    {
      pixel_out_t ret;

      value_type acc;
      for(size_t i = 0; i < pixel_out_t::dimension::value; i++)
      {
        acc = 0;
        for(size_t j = 0; j < pixel_in_t::dimension::value; j++)
        {
          acc += this->matrix_transform[i][j] * u[j];
        }
        ret[i] = acc;
      }
      return ret;
    }
  };


  /*!@brief Specialization of s_matrix_pixel_transformation for pixel_3
   */
  template <
    class value_in_t,
    class value_out_t,
    class matrix_element_t /* = yaF_simple */>
  struct s_matrix_pixel_transformation<
    s_compound_pixel_t< value_in_t, mpl::int_<3> >,
    s_compound_pixel_t< value_out_t, mpl::int_<3> >,
    matrix_element_t>
    : public impl::s_matrix_pixel_transformation_base<
        s_compound_pixel_t< value_in_t, mpl::int_<3> >,
        s_compound_pixel_t< value_out_t, mpl::int_<3> >,
        matrix_element_t>
  {
    using pixel_in_t = s_compound_pixel_t< value_in_t, mpl::int_<3> >;
    using pixel_out_t = s_compound_pixel_t< value_out_t, mpl::int_<3> >;
    using base_t = impl::s_matrix_pixel_transformation_base<pixel_in_t, pixel_out_t, matrix_element_t>;
    using value_type = typename base_t::value_type;

    pixel_out_t operator()(pixel_in_t const& u) const noexcept
    {
      return
        pixel_out_t(
          static_cast<value_type>(this->matrix_transform[0][0] * u.a + this->matrix_transform[0][1] * u.b + this->matrix_transform[0][2] * u.c),
          static_cast<value_type>(this->matrix_transform[1][0] * u.a + this->matrix_transform[1][1] * u.b + this->matrix_transform[1][2] * u.c),
          static_cast<value_type>(this->matrix_transform[2][0] * u.a + this->matrix_transform[2][1] * u.b + this->matrix_transform[2][2] * u.c));
    }
  };


  /*!@brief Generic pixel transform operator from RGB to YCbCr
   *
   * Applies a generic transformation from RGB to YCbCr, defined only by the
   * 2 constants for R and B.
   * See https://en.wikipedia.org/wiki/YCbCr for more details.
   */
  template <class pixel_in_t, class pixel_out_t>
  struct s_RGB_to_YCbCr_generic : public s_matrix_pixel_transformation<pixel_in_t, pixel_out_t>
  {
    static_assert(pixel_in_t::dimension::value == 3, "Only for pixel3");
    static_assert(pixel_out_t::dimension::value == 3, "Only for pixel3");

    s_RGB_to_YCbCr_generic(double luma_r, double luma_b)
    {
      this->matrix_transform[0][0]   = luma_r;
      this->matrix_transform[0][1]   = 1 - luma_r - luma_b;
      this->matrix_transform[0][2]   = luma_b;

      this->matrix_transform[1][0] = 0.5;
      this->matrix_transform[1][1] = (-1 + luma_r + luma_b)/(2*(1-luma_r));
      this->matrix_transform[1][2] = - luma_b / (2*(1-luma_r));

      this->matrix_transform[2][0] = - luma_r / (2*(1-luma_b));
      this->matrix_transform[2][1] = (-1 + luma_r + luma_b)/(2*(1-luma_b));
      this->matrix_transform[2][2] = 0.5;
    }
  };


  //! Functor implementing the transformation from RGB to YUV Rec601
  //!
  //! The function object defines the Rec 601 variant of the YUV color space and
  //! transformation.
  //! See https://en.wikipedia.org/wiki/Rec._601 for more details.
  template<class pixel_in_t, class pixel_out_t>
  struct s_RGB_to_YUV_Rec_601 : s_RGB_to_YCbCr_generic<pixel_in_t, pixel_out_t>
  {
    s_RGB_to_YUV_Rec_601() : s_RGB_to_YCbCr_generic<pixel_in_t, pixel_out_t>(0.299, 0.114)
    {}
  };


  //! Functor implementing the transformation from RGB to YUV Rec709
  //!
  //! The function object defines the Rec 709 variant of the YUV color space and
  //! transformation.
  //! See https://en.wikipedia.org/wiki/YCbCr for more details.
  template<class pixel_in_t, class pixel_out_t>
  struct s_RGB_to_YUV_Rec_709 : s_RGB_to_YCbCr_generic<pixel_in_t, pixel_out_t>
  {
    s_RGB_to_YUV_Rec_709() : s_RGB_to_YCbCr_generic<pixel_in_t, pixel_out_t>(0.2126, 0.0722)
    {}
  };




  /*! Implements a generic pixel transform from RGB to YUV
   *
   * The genericity comes from the fact that the luminance value for each chanels
   * and the ranges may be specified.
   *
   *  R = Y + Cr*(2 - 2*LumaRed)
   *  B = Y + Cb*(2 - 2*LumaBlue)
   *  G =   Y
   *        - LumaBlue*Cb*(2-2*LumaBlue)/LumaGreen
   *        - LumaRed*Cr*(2-2*LumaRed)/LumaGreen
   *
   */
  //! Functor implementing the reverse transformation of RGB to YUV
  template<class pixel_in_t, class pixel_out_t>
  struct s_YCbCr_to_RGB_generic : public s_matrix_pixel_transformation<pixel_in_t, pixel_out_t>
  {
    static_assert(pixel_in_t::dimension::value == 3, "Only for pixel3");
    static_assert(pixel_out_t::dimension::value == 3, "Only for pixel3");

    s_YCbCr_to_RGB_generic(double luma_r, double luma_b)
    {
      this->matrix_transform[0][0] = 1;
      this->matrix_transform[0][1] = 2 * (1-luma_r);
      this->matrix_transform[0][2] = 0;

      this->matrix_transform[1][0] = 1;
      this->matrix_transform[1][1] = -2*luma_r*(1-luma_r)/(1 - luma_r - luma_b);
      this->matrix_transform[1][2] = -2*luma_b*(1-luma_b)/(1 - luma_r - luma_b);

      this->matrix_transform[2][0] = 1;
      this->matrix_transform[2][1] = 0;
      this->matrix_transform[2][2] = 2 * (1-luma_b);

    }
  };


  //! Functor implementing the transformation from RGB to YUV Rec601
  //!
  //! The function object defines the Rec 601 variant of the YUV color space and
  //! transformation.
  //! See https://en.wikipedia.org/wiki/Rec._601 for more details.
  template<class pixel_in_t, class pixel_out_t>
  struct s_YUV_Rec_601_to_RGB : public s_YCbCr_to_RGB_generic<pixel_in_t, pixel_out_t>
  {

    s_YUV_Rec_601_to_RGB() : s_YCbCr_to_RGB_generic<pixel_in_t, pixel_out_t>(0.299, 0.114)
    {}

    // it is possible to reduce a bit the number of multiplications, as the
    // matrix contains 0.
#if 0
    pixel_out_t operator()(pixel_in_t const& u) const throw()
    {
      typedef typename pixel_out_t::value_type value_type;
      
      // Refaire la matrice d'inversion
      static const yaF_simple 
        c11 = 1.f,      c12 = -0.000039f, c13 = 1.13983f,
        c21 = 1.00039f, c22 = -0.39381f,  c23 = -0.580501f,
        c31 = 0.997972f,c32 = 2.02788f,   c33 = -0.0004804f;
        
      return 
        pixel_out_t(
          static_cast<value_type>(c11 * u.a + c12 * u.b + c13 * u.c),
          static_cast<value_type>(c21 * u.a + c22 * u.b + c23 * u.c),
          static_cast<value_type>(c31 * u.a + c32 * u.b + c33 * u.c));
    }
#endif
  };

  //! Functor implementing the transformation from YUV to RGB Rec709
  //!
  //! The function object defines the Rec 709 variant of the YUV color space and
  //! transformation.
  //! See https://en.wikipedia.org/wiki/YCbCr for more details.
  template<class pixel_in_t, class pixel_out_t>
  struct s_YUV_Rec_709_to_RGB : public s_YCbCr_to_RGB_generic<pixel_in_t, pixel_out_t>
  {

    s_YUV_Rec_709_to_RGB() : s_YCbCr_to_RGB_generic<pixel_in_t, pixel_out_t>(0.2126, 0.0722)
    {}
  };


  //! Transforms an RGB image into an YUV image
  //!
  //! @anchor color_RGB_to_YUV_Rec_601_t
  //! See @ref s_RGB_to_YUV_Rec_601 for more details on the transformation.
  template <class image_in_, class image_out_>
  yaRC color_RGB_to_YUV_Rec_601_t(const image_in_& imin, image_out_& imo)
  {
    typedef s_RGB_to_YUV_Rec_601<
      typename image_in_::pixel_type, 
      typename image_out_::pixel_type>  operator_type;
    
    s_apply_unary_operator op_processor;
    
    operator_type op;
    yaRC ret = op_processor(imin, imo, op);
    if(ret == yaRC_ok)
    {
      imo.ColorSpace() = cs_YCbCr601;
    }
    return ret;
  }


  //! Transforms an YUV image into an RGB image
  //!
  //! This is the reverse transform of @ref color_RGB_to_YUV_Rec_601_t.
  //! See @ref s_YUV_Rec_601_to_RGB for more details on the transformation.
  template <class image_in_, class image_out_>
  yaRC color_YUV_Rec_601_to_RGB_t(const image_in_& imin, image_out_& imo)
  {
    typedef s_YUV_Rec_601_to_RGB<
      typename image_in_::pixel_type, 
      typename image_out_::pixel_type>  operator_type;
    
    s_apply_unary_operator op_processor;
    
    operator_type op;
    return op_processor(imin, imo, op);
  }





  //! @} doxygroup: pp_color_details_grp


}

#endif /* YAYI_COLORPROCESS_YUV_T_HPP__ */
