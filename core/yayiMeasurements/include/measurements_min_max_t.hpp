#ifndef YAYI_MEASUREMENTS_MIN_MAX_T_HPP__
#define YAYI_MEASUREMENTS_MIN_MAX_T_HPP__

#include <boost/call_traits.hpp>
#include <functional>
//#include <boost/limits.hpp>
#include <utility>
#include <boost/numeric/conversion/bounds.hpp>

#include <yayiImageCore/include/ApplyToImage_unary_t.hpp>


namespace yayi
{
  namespace measurements
  {
    /*!@defgroup meas_min_max_details_grp Min/max template functions
     * @ingroup meas_details_grp
     * @{
     */

    //!@concept{measurement_functor_concepts}
    template <class T>
    struct s_meas_min_max
    {
      typedef std::pair<T, T> result_type;

      T min_, max_;
      s_meas_min_max() : min_(boost::numeric::bounds<T>::highest()), max_(boost::numeric::bounds<T>::lowest()) {}
      void operator()(const T& v) noexcept
      {
        min_ = std::min(v, min_);
        max_ = std::max(v, max_);
      }

      result_type result() const noexcept
      {
        return std::make_pair(min_, max_);
      }

      void clear_result() noexcept
      {
        min_ = boost::numeric::bounds<T>::highest();
        max_ = boost::numeric::bounds<T>::lowest();
      }
    };

    template <class T, int K>
    struct s_meas_min_max<s_compound_pixel_t<T, mpl::int_<K>>>
    {
      using pixel_t = s_compound_pixel_t<T, mpl::int_<K>>;
      using scalar_meas_min_max = s_meas_min_max<T>;
      scalar_meas_min_max op_seq[K];

      // would be better to transform to tuple, but this is not
      // supported by the variants yet
      typedef std::vector<typename scalar_meas_min_max::result_type> result_type;

      s_meas_min_max() {}

    private:
      template <std::size_t ...I>
      void pack_call(const pixel_t& v, std::index_sequence<I...>) noexcept {
        using dummy=int[];
        (void)dummy{ ((void)op_seq[I](v[I]), 0)... };
      }

      template <std::size_t ...I>
      result_type pack_result(std::index_sequence<I...>) const noexcept {
        return result_type{op_seq[I].result() ...};
      }

    public:

      void operator()(const pixel_t& v) noexcept
      {
        pack_call(v, std::make_index_sequence<K>());
      }

      result_type result() const noexcept
      {
        return pack_result(std::make_index_sequence<K>());
      }

      void clear_result() noexcept
      {
        for(int i = 0; i < K; i++)
          op_seq[i].clear_result();
      }
    };


    //!@todo get rid of the variant
    template <class image_in_t>
    yaRC image_meas_min_max_t(const image_in_t& im, variant& out)
    {
      typedef s_meas_min_max<
        typename image_in_t::pixel_type> operator_type;

      s_apply_unary_operator op_processor;

      operator_type op;
      yaRC res = op_processor(im, op);
      if(res != yaRC_ok)
      {
        DEBUG_INFO("An error occured during the computation of the min/max: " << res);
        return res;
      }

      out = op.result();
      return yaRC_ok;
    }
    //! @}

  }
}


#endif /* YAYI_MEASUREMENTS_MIN_MAX_T_HPP__ */
