


#include <yayiImageCore/include/yayiImageCore.hpp>
#include <yayiImageCore/include/yayiImageCore_Impl.hpp>

#include <boost/mpl/lambda.hpp>
#include <iostream>


namespace yayi
{
  namespace impl_
  {
    template <type::compound_type e, int dimension>
    IImage* image_factory_outer_type_helper(const type& c)
    {
      switch(c.s_type)
      {
      case type::s_ui8:
        return new Image<from_type<e, type::s_ui8>, s_coordinate<dimension> >();
      case type::s_i8:
        return new Image<from_type<e, type::s_i8>, s_coordinate<dimension> >();
      case type::s_ui16:
        return new Image<from_type<e, type::s_ui16>, s_coordinate<dimension> >();
      case type::s_i16:
        return new Image<from_type<e, type::s_i16>, s_coordinate<dimension> >();
      case type::s_ui32:
        return new Image<from_type<e, type::s_ui32>, s_coordinate<dimension> >();
      case type::s_i32:
        return new Image<from_type<e, type::s_i32>, s_coordinate<dimension> >();
      case type::s_ui64:
        return new Image<from_type<e, type::s_ui64>, s_coordinate<dimension> >();
      case type::s_i64:
        return new Image<from_type<e, type::s_i64>, s_coordinate<dimension> >();
      case type::s_float:
        return new Image<from_type<e, type::s_float>, s_coordinate<dimension> >();
      case type::s_double:
        return new Image<from_type<e, type::s_double>, s_coordinate<dimension> >();
      default:
        YAYI_THROW("Unsupported scalar type");
        return 0;

      }

      return 0;
    }

    template <type::compound_type e, int dimension>
    IImage* image_factory_outer_type_helper_only_float_types(const type& c)
    {
      switch(c.s_type)
      {
      case type::s_float:
        return new Image<from_type<e, type::s_float>, s_coordinate<dimension> >();
      case type::s_double:
        return new Image<from_type<e, type::s_double>, s_coordinate<dimension> >();
      default:
        YAYI_THROW("Unsupported scalar type");
        return 0;

      }

      return 0;
    }


    template <int dim>
    IImage* image_factory_helper_dimension(const type &c)
    {
      switch(c.c_type)
      {
      case type::c_scalar:
        return image_factory_outer_type_helper<type::c_scalar, dim>(c);
      case type::c_complex:
        return image_factory_outer_type_helper_only_float_types<type::c_complex, dim>(c);
      case type::c_3:
        return image_factory_outer_type_helper<type::c_3, dim>(c);
      case type::c_4:
        return image_factory_outer_type_helper<type::c_4, dim>(c);

      default:
          YAYI_THROW("Unsupported compound type");
          return 0;
      }
    }
  } // namespace impl_


  IImage* IImage::Create(type c, yaUINT8 dimension)
  {

    switch(dimension)
    {
      case 0:
      case 1:
        YAYI_THROW("Cannot create an image of dimension 0 or 1");
        return 0;

      case 2: return impl_::image_factory_helper_dimension<2>(c);
      case 3: return impl_::image_factory_helper_dimension<3>(c);
      case 4: return impl_::image_factory_helper_dimension<4>(c);
      default:
        YAYI_THROW("Unsupported dimension");
        return 0;
    }
  }


}

