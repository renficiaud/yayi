#ifndef YAYI_COMMON_HYPER_RECTANGLE_HPP__
#define YAYI_COMMON_HYPER_RECTANGLE_HPP__


/*!@file
 * This file defines the hyperrectangle structure, used to define subspaces in images
 * @author Raffi Enficiaud
 */

#include <yayiCommon/common_coordinates.hpp>
#include <boost/operators.hpp>

namespace yayi
{

  /*!@brief Rectangle in any dimension
   * @ingroup common_coord_grp
   *
   * An hyperrectangle is an extension of the classical 2D rectangle to any dimension. In order to fully define a rectangle,
   * two coordinates are required. 
   * @author Raffi Enficiaud
   */
  template <int dim> 
  struct s_hyper_rectangle :
    boost::equality_comparable< s_hyper_rectangle<dim> >
  {
    typedef s_hyper_rectangle<dim> this_type;

    //! Coordinate type used by the hyperrectangle
    typedef s_coordinate<dim> coordinate_type;

    //! The first corner of the hyperrectangle. This is the corner closest to the origin of the 
    //! referential.
    coordinate_type lowerleft_corner, upperright_corner;

    //! Hyperrectangle default constructor
    s_hyper_rectangle() : lowerleft_corner(), upperright_corner()
    {}

    //! Copy constructs an hyperrectangle
    s_hyper_rectangle(const this_type& r) :
      lowerleft_corner(r.lowerleft_corner), upperright_corner(r.upperright_corner)
    {}
    
    //! Constructs an hyperrectangle from an origin and a size
    s_hyper_rectangle(const coordinate_type& lower_left, const coordinate_type& size) :
      lowerleft_corner(lower_left), upperright_corner(size + lower_left)
    {
      YAYI_ASSERT(lowerleft_corner.dimension() == upperright_corner.dimension(), "Corners of different dimension");
    }
    
    //! Returns the size spanned by the hyperrectangle
    coordinate_type Size() const noexcept
    {
      return upperright_corner - lowerleft_corner;
    }

    //! Sets the size of the hyperrectangle
    void SetSize(const coordinate_type& size) noexcept
    {
      upperright_corner = size + lowerleft_corner;
    }

    //! Sets the origin of the hyperrectangle, preserves the size
    void SetOrigin(const coordinate_type& origin) noexcept
    {
      coordinate_type s = Size();
      lowerleft_corner = origin;
      upperright_corner = s + lowerleft_corner;
    }

    //! Returns the origin of the hyperrectangle (lowerleft corner)
    coordinate_type const& Origin() const noexcept
    {
      return lowerleft_corner;
    }
    
    //! Returns true if the argument hyperrectangle has the same geometry, false otherwise
    bool operator==(const this_type& r) const throw()
    {
      return &r == this || (r.lowerleft_corner == lowerleft_corner && r.upperright_corner == upperright_corner);
    }
    
    //! Returns true if the specified point is inside the rectangle
    bool is_inside(const coordinate_type& c) const
    {
      YAYI_ASSERT(lowerleft_corner.dimension() == c.dimension(), "incompatible dimensions");
      for(unsigned int i = 0; i < lowerleft_corner.dimension(); i++)
      {
        const typename coordinate_type::scalar_coordinate_type p = c[i];
        if(p < lowerleft_corner[i] || p >= upperright_corner[i])
          return false;
      }
      return true;
    }
    
    //! Returns true if the specified rectangle is inside this rectangle
    bool is_inside(const this_type& rect) const
    {
      return is_inside(rect.lowerleft_corner) && is_inside(rect.upperright_corner - 1);
    }  
      
    //! Returns true if the specified rectangle intersects this one
    bool intersects(const this_type& rect) const
    {
      return is_inside(rect.lowerleft_corner) || is_inside(rect.upperright_corner - 1);
    }    
  };
 
}

namespace std
{
  template <int dim>
  struct hash< yayi::s_hyper_rectangle<dim> >
  {
    size_t operator()(yayi::s_hyper_rectangle<dim> const& rect) const
    {
      size_t seed = 0;
      // borrowed from boost, who borrowed from ...
      seed ^= std::hash<yayi::s_coordinate<dim> >()(rect.lowerleft_corner) + 0x9e3779b9 + (seed<<6) + (seed>>2);
      seed ^= std::hash<yayi::s_coordinate<dim> >()(rect.upperright_corner) + 0x9e3779b9 + (seed<<6) + (seed>>2);
      return seed;
    }
  };

}

#endif /* YAYI_COMMON_HYPER_RECTANGLE_HPP__ */
