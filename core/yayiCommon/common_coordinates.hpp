#ifndef YAYI_COMMON_COORDINATES_HPP__
#define YAYI_COMMON_COORDINATES_HPP__

/*!@file
 * This file contains the coordinates structure, which is a template class parametrized by the dimension of the space.
 * It contains also some usefull functions for manipulating coordinates.
 * @author Raffi Enficiaud
 */


#include <yayiCommon/common_types.hpp>
#include <yayiCommon/common_errors.hpp>
#include <vector>
#include <algorithm>
#include <boost/functional/hash.hpp>
#include <boost/operators.hpp>


namespace yayi
{
  /*!@defgroup common_coord_grp Storing and manipulating coordinates
   * @ingroup common_grp
   *
   * Coordinates are handled by the class yayi::s_coordinate which has arithmetic semantics
   * wrt. other coordinates and scalar values. A special case is coordinates of dimension 0, which
   * are used as coordinates with dynamically set dimensions.
   * @{
   */


  // Forward declarations
  template <int dim, class scalar_coordinate_t> struct s_coordinate;
  template <class scalar_coordinate_t>          struct s_coordinate<0, scalar_coordinate_t>;


  /*!@brief Main coordinates type.
   *
   * @tparam dim the dimension spanned by the coordinate
   * @tparam scalar_coordinate_t the type encoding each dimension of the geometry
   *
   * This class encodes coordinates representations. The dimension is specified at compilation time, as well as
   * the type encoding each dimension. By default, this type is of integer class, which is appropriate for
   * spaces in grid, such as the ones in discrete images. It is also possible to provide another type, which should
   * implement the "strict equality", "strict inequality" and "less ordering" operations.
   * @author Raffi Enficiaud
   */
  template <
    int dim, 
    class scalar_coordinate_t = scalar_coordinate>
  struct s_coordinate :
    boost::additive< s_coordinate<dim, scalar_coordinate_t>
    , boost::totally_ordered< s_coordinate<dim, scalar_coordinate_t>
    , boost::additive< s_coordinate<dim, scalar_coordinate_t>, scalar_coordinate
    , boost::totally_ordered< s_coordinate<dim, scalar_coordinate_t>, scalar_coordinate
  > > > >
  {
  public:

    typedef scalar_coordinate_t scalar_coordinate_type;		//!< Type contained in each dimension
    enum {static_dimensions = dim};

  private:
    std::array<scalar_coordinate_type, dim> coords;
    typedef s_coordinate<dim, scalar_coordinate_t> this_type;
    
    friend struct std::hash<this_type>;

  public:
    s_coordinate()
    {}
    
    s_coordinate(std::array<scalar_coordinate_t, dim> const &t) : coords(t)
    {}

    s_coordinate(std::initializer_list<scalar_coordinate_t> const &t)
    {
      YAYI_ASSERT(t.size() == dim, "wrong number of arguments");
      std::copy(std::begin(t), std::end(t), std::begin(coords));
    }

    s_coordinate(const this_type& r) : coords(r.coords)
    {}
    
    s_coordinate(const scalar_coordinate_type r)
    {
      coords.fill(r);
    }

    explicit s_coordinate(const s_coordinate<0, scalar_coordinate_type>& r);



    //! @brief Constructs a coordinate from a forward iterator.
    //!
    //! May be used directly from C/C++ tables
    //! @pre The iterator implements the forward iterator concept, and the passed argument is incrementable at least "dim" times.
    //! @warning It is impossible to check the proper size of the pointed container inside the function.
    template <class it_t>
    static this_type from_table(it_t it)
    {
      this_type out;
      for(int i = 0; i < dim; i++)
        out.coords[i] = *it++;
      return out;
    }

    //! Constructs several coordinates from a collection of "tables"
    template <class it_t>
    static std::vector<this_type> from_table_multiple(it_t r_begin, const it_t r_end) YAYI_THROW_DEBUG_ONLY__
    {
      DEBUG_ASSERT(((r_end - r_begin) % dim) == 0, "Incorrect number of elements");
      std::vector<this_type> out;
      for(; r_begin != r_end; r_begin += dim)
        out.push_back(from_table(r_begin));
      return out;
    }

    //! Returns true if the coordinate is set to the origin of the space
    bool is_origin() const noexcept
    {
      return ((*this) == 0);
    }

    //! Index const access to a particular dimension of the coordinate
    scalar_coordinate_type operator[](unsigned int i) const YAYI_THROW_DEBUG_ONLY__
    {
      DEBUG_ASSERT(i < dim, "Error out of range");
      return coords[i];
    }
    //! Index access to a particular dimension of the coordinate
    scalar_coordinate_type& operator[](unsigned int i) YAYI_THROW_DEBUG_ONLY__
    {
      DEBUG_ASSERT(i < dim, "Error out of range");
      return coords[i];
    }

    //! Sets the coordinate to the origin of the space
    void clear() noexcept
    {
      coords.fill(0);
    }

    //! For compatibility with s_coordinate<0> ... bad design
    void set_dimension(int) noexcept {}

    //! The coordinate dimension
    constexpr unsigned int dimension() const noexcept
    {
      return static_cast<unsigned int>(dim);
    }


    //!@name Operations with other coordinates
    //!@{

    //! Comparison between two instances of coordinates
    bool operator==(const this_type& r) const noexcept
    {
      return coords == r.coords;
    }

    /*! Simple lexicographical order among coordinates of the same type
     */
    bool operator<(const this_type& r) const noexcept {
      for(int i = 0; i < dim; i++)
      {
        if(coords[i] == r.coords[i])
          continue;
        return coords[i] < r.coords[i];
      }
      return false;
    }

    //! Shift/offset
    this_type& operator+=(const this_type& r) noexcept
    {
      for(int i = 0; i < dim; i++)
        coords[i] += r.coords[i];
      return *this;
    }

    //! Shift
    this_type& operator-=(const this_type& r) noexcept
    {
      for(int i = 0; i < dim; i++) coords[i] -= r.coords[i];
      return *this;
    }

    //!@}

    //!@name Operations with scalar values
    //!@{

    //! Compare the coordinates to a scalar value
    bool operator==(const scalar_coordinate_type v) const noexcept
    {
      for(int i = 0; i < dim; i++)
      {
        if(coords[i] != v) 
          return false;
      }
      return true;
    }

    bool operator<(const scalar_coordinate_type& r) const noexcept {
      for(int i = 0; i < dim; i++)
      {
        if(coords[i] == r)
          continue;
        return coords[i] < r;
      }
      return false;
    }

    this_type& operator+=(const scalar_coordinate_type& r) noexcept
    {
      for(int i = 0; i < dim; i++)
        coords[i] += r;
      return *this;
    }


    this_type& operator-=(const scalar_coordinate_type& r) noexcept
    {
      for(int i = 0; i < dim; i++)
        coords[i] -= r;
      return *this;
    }

    //!@}



    //! Returns the "min" of two coordinates, which is the dimensionwise minimum
    friend this_type min_coordinate(this_type l, const this_type& r)
    {
      for(int i = 0; i < dim; i++)
        l[i] = std::min(l[i], r[i]);
      return l;
    }

  };


  /*!@brief Specializing of coordinates for unknown (any) dimension
   *
   * @author Raffi Enficiaud
   */
  template <class scalar_coordinate_t>
  struct s_coordinate<0, scalar_coordinate_t> :
    boost::additive< s_coordinate<0, scalar_coordinate_t>
    , boost::totally_ordered< s_coordinate<0, scalar_coordinate_t>
    , boost::additive< s_coordinate<0, scalar_coordinate_t>, scalar_coordinate
    , boost::totally_ordered< s_coordinate<0, scalar_coordinate_t>, scalar_coordinate
  > > > >
  {
    //! Storage class of the coordinates
    typedef scalar_coordinate_t scalar_coordinate_type;

  private:
    std::vector<scalar_coordinate_type> coords;
    typedef s_coordinate<0, scalar_coordinate_t> this_type;

  public:

    //! Default constructor
    s_coordinate() : coords()
    {}


    //! Copy constructor
    s_coordinate(const this_type& r) : coords(r.coords.begin(), r.coords.end())
    {
    }

    //! Copy constructor from a coordinate of diffferent dimension
    template <int i_dim>
      s_coordinate(const s_coordinate<i_dim>& r) : coords()
    {
      coords.resize(r.dimension());
      for(int i = 0, j = r.dimension(); i < j; i++)
        coords[i] = r[i];
    }


    //!@name Operations with coordinates
    //!@{

    /*! Simple lexicographical order between two coordinates of the same type
     *  Intended for convenience
     */
    bool operator<(const this_type& r) const noexcept
    {
      if(dimension() != r.dimension())
        return dimension() < r.dimension();
      return coords < r.coords;
    }

    //! Comparison between two instances of coordinates
    bool operator==(const this_type& r) const noexcept
    {
      return coords == r.coords;
    }

    //! Shift/translation
    this_type& operator+=(const this_type& r)
    {
      // Raffi : il faut changer ce comportement
      for(int i = 0, j = std::min(dimension(), r.dimension()); i < j; i++)
        coords[i] += r[i];
      if(dimension() < r.dimension())
      {
        for(int i = dimension(), j = r.dimension(); i < j; i++)
          coords.push_back(r[i]);
      }
      return *this;
    }

    //! Shift/translation
    this_type& operator-=(const this_type& r)
    {
      // Raffi : il faut changer ce comportement
      for(int i = 0, j = std::min(dimension(), r.dimension()); i < j; i++)
        coords[i] -= r[i];
      if(dimension() < r.dimension())
      {
        for(int i = dimension(), j = r.dimension(); i < j; i++)
          coords.push_back(-r[i]);
      }
      return *this;
    }

    //!@}

    //!@name Operations with scalars
    //!@{

    //! Compare the coordinates to a scalar value
    bool operator==(const scalar_coordinate_type v) const noexcept
    {
      for(int i = 0, j = dimension(); i < j; i++)
      {
        if(coords[i] != v)
          return false;
      }
      return true;
    }

    //! Compare the coordinates to a scalar value
    bool operator<(const scalar_coordinate_type v) const noexcept
    {
      for(int i = 0, j = dimension(); i < j; i++)
      {
        if(!(coords[i] < v))
          return false;
      }
      return true;
    }

    this_type& operator+=(const scalar_coordinate_type r) noexcept
    {
      for(int i = 0, j = dimension(); i < j; i++)
        coords[i] += r;
      return *this;
    }

    this_type& operator-=(const scalar_coordinate_type r) noexcept
    {
      for(int i = 0, j = dimension(); i < j; i++)
        coords[i] -= r;
      return *this;
    }

    //!@}

    //! Returns the "min" of two coordinates, which is the dimensionwise minimum
    //! The dimension of the output vector is the maximal dimension of both vector, the missing dimensions
    //! are considered as 0.
    friend this_type min_coordinate(this_type l, const this_type& r)
    {
      for(int i = 0, j = std::min(l.dimension(), r.dimension()); i < j; i++)
        l[i] = std::min(l[i], r[i]);
      if(l.dimension() < r.dimension())
      {
        for(int i = l.dimension(), j = r.dimension(); i < j; i++)
          l.coords.push_back(0);
      }
      else
      {
        for(int i = r.dimension(), j = l.dimension(); i < j; i++)
          l.coords[i] = 0;
      }
      return l;
    }

    //! Index operator (const)
    const scalar_coordinate_type& operator[](unsigned int i) const
    {
      return coords[i];
    }

    //! Index operator (non const)
    scalar_coordinate_type& operator[](unsigned int i)
    {
      return coords[i];
    }


    //! Returns true if the coordinate is set to the origin of the space
    bool is_origin() const noexcept
    {
      return (*this) == 0;
    }

    //! Returns the number of dimensions (dynamic)
    const unsigned int dimension() const noexcept
    {
      return static_cast<unsigned int>(coords.size());
    }

    //! Returns the number of dimensions (dynamic)
    void set_dimension(int d) noexcept
    {
      coords.resize(d);
    }

    //! Resets the coordinate
    void clear() noexcept
    {
      coords.clear();
    }

  };


  template <int dim, class scalar_coordinate_t>
  s_coordinate<dim, scalar_coordinate_t>::s_coordinate(const s_coordinate<0, scalar_coordinate_t>& r)
  {
    unsigned int i = 0;
    for(unsigned int j = std::min(r.dimension(), static_cast<unsigned int>(dim)); i < j; i++) coords[i] = r[i];
    if(i < dim)
    {
      for(; i < dim; i++)
      {
        coords[i] = 0;
      }
    }
    else
    {
      for(unsigned int j = r.dimension(); i < j; i++)
      {
        DEBUG_ASSERT(
          r[i] == 0,
          "Trying to copy a generic coordinate of dimension " + int_to_string(r.dimension())
          + std::string(" into a coordinate of dimension ") + int_to_string(dim));
      }
    }
  }

  //! Creates a coordinates of 4 dimensions
  inline s_coordinate<4> c4D(
    const s_coordinate<4>::scalar_coordinate_type a,
    const s_coordinate<4>::scalar_coordinate_type b,
    const s_coordinate<4>::scalar_coordinate_type c,
    const s_coordinate<4>::scalar_coordinate_type d) {

    const s_coordinate<4>::scalar_coordinate_type t[4] = {a,b,c,d};
    return s_coordinate<4>::from_table(t);
  }

  //! Creates a coordinates of 3 dimensions
  inline s_coordinate<3> c3D(
    const s_coordinate<3>::scalar_coordinate_type x,
    const s_coordinate<3>::scalar_coordinate_type y,
    const s_coordinate<3>::scalar_coordinate_type z) {

    const s_coordinate<3>::scalar_coordinate_type t[3] = {x,y,z};
    return s_coordinate<3>::from_table(t);
  }

  //! Creates a coordinates of 2 dimensions
  inline s_coordinate<2> c2D(
    const s_coordinate<2>::scalar_coordinate_type x,
    const s_coordinate<2>::scalar_coordinate_type y) {

    const s_coordinate<2>::scalar_coordinate_type t[2] = {x,y};
    return s_coordinate<2>::from_table(t);
  }

  //! Streaming of coordinates
  template <int dim, class T>
  inline std::ostream & operator<<(std::ostream &o, const yayi::s_coordinate<dim, T> &c)
  {
    int i = 0, j = c.dimension() - 1;
    o << "(";
    for(; i < j; i++)
    {
      o << c[i] << ", ";
    }
    if(j >= 0) o << c[j];
    o << ")";
    return o;
  }

  //! @} // common_coord_grp
}


namespace std
{
  template <
    int dim, 
    class scalar_coordinate_t>
  struct hash< yayi::s_coordinate<dim, scalar_coordinate_t> >
  {
    size_t operator()(yayi::s_coordinate<dim, scalar_coordinate_t> const& c) const
    {
      return boost::hash_range(std::begin(c.coords), std::end(c.coords));;
    }
  };

}

#endif


