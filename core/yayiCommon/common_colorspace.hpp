#ifndef YAYI_COMMON_COLORSPACE_HPP__
#define YAYI_COMMON_COLORSPACE_HPP__

/*!@file
 * This file contains the necessary elements for colour information. It contains also several functions for the computation
 * of spectral information (blackbody, gamut)...
 *
 * @author Raffi Enficiaud
 */

#include <yayiCommon/common_types.hpp>
#include <yayiCommon/common_constants.hpp>

namespace yayi
{
  /*!@defgroup common_color_grp Colour 
   * @brief Color spaces and utilities.
   * @ingroup common_grp
   * @{
   */


  //! Standard observer tables structure
  struct s_standard_observer_data
  {
    yaUINT32      nanometer;
    yaF_double    x,y,z;
  };

  //! The CIE 1931 2 degrees standard observer table (within the range [380, 780] nm).
  YCom_ extern s_standard_observer_data const xyz_CIE_1931[];


  /*!@brief Structure encoding the colour space (which should be associated to an image)
   *
   * @author Raffi Enficiaud
   */
  struct color_space
  {
    //! @internal
    typedef color_space this_type;
    
    //! The major category of the color space
    typedef enum e_colorspace_major
    {
      ecd_undefined,    //!< Undefined color space
      ecd_rgb,          //!< Color space is of class RGB
      ecd_hls,          //!< Color space is of class HLS
      ecd_lab,          //!< Color space is of class La*b*
      ecd_xyz,          //!< Color space is of class XYZ
      ecd_xyY,          //!< Color space is of class xyY
      ecd_yuv,          //!< Color space is of class YUV
      ecd_ycbcr,        //!< Color space is of class YCbCr
      ecd_cmyk,         //!< Color space is of class CMYK
    } yaColorSpaceMajor;

    //! The minor category of the color space (variation/implementation of the major color space)
    typedef enum e_colorspace_minor
    {
      ecdm_undefined,               //!< Undefined subcategory
      ecdm_hls_l1,                  //!< The HLS color space follows the \f$\ell_1\f$ norm
      ecdm_hls_trig,                //!< The HLS color space follows the trigonometric definition
      ecdm_ycbcr_rec601,            //!< Follows the specs of the Rec 601 (https://en.wikipedia.org/wiki/YCbCr#ITU-R_BT.601_conversion)
      ecdm_ycbcr_spec_illuminants,  //!< Has its own illuminants
      ecdm_lab_itu                  //!< CIE LAB ITU variant (no known source)
    } yaColorSpaceMinor;

    //! Standard illuminants
    typedef enum e_illuminant
    {
      ei_undefined,     //!< Undefined illuminant
      ei_d50,           //!< D50 illuminant
      ei_d55,           //!< D55 illuminant
      ei_d65,           //!< D65 illuminant
      ei_d75,           //!< D75 illuminant
      ei_A,             //!< Illuminant CIE "A" definition
      ei_B,             //!< Illuminant CIE "B" definition
      ei_C,             //!< Illuminant CIE "C" definition
      ei_E,             //!< Illuminant CIE "E" definition
      ei_blackbody      //!< The illuminant is defined from the corresponding blackbody radiator temperature
    } yaIlluminant;

    //! RGB standard primaries used to define the RGB color space
    typedef enum e_rgb_primaries
    {
      ergbp_undefined,  //!< Undefined primaries
      ergbp_CIE,        //!< CIE standard primaries
      ergbp_sRGB,       //!< sRGB primaries
      ergbp_AdobeRGB,   //!< Adobe RGB primaries
      ergbp_AppleRGB,   //!< Apple RGB primaries
      ergbp_NTSCRGB,    //!< NTSC primaries
      ergbp_SecamRGB    //!< Secam primaries
    } yaRGBPrimary;

    yaColorSpaceMajor cs_major;
    yaColorSpaceMinor cs_minor;
    yaIlluminant      illuminant;
    yaRGBPrimary      primary;
    
    
    //! Color space equality operator
    bool operator==(this_type const & r) const noexcept {
      return cs_major == r.cs_major &&
        cs_minor == r.cs_minor &&
        illuminant == r.illuminant &&
        primary == r.primary;
    }

    bool operator!=(this_type const & r) const noexcept {
      return !((*this) == r);
    }

    //! Stringifier
    YCom_ operator string_type() const noexcept;
    
    //! Streaming
    template <class stream_t>
    friend stream_t& operator<<(stream_t& o, const this_type& t) noexcept {
      o << t.operator string_type(); return o;
    }
    
    
    //! Default constructor
    color_space() 
    : cs_major(ecd_undefined),
      cs_minor(ecdm_undefined),
      illuminant(ei_undefined),
      primary(ergbp_undefined)
    {}
    
    //! Direct constructor
    color_space(
      yaColorSpaceMajor mm,
      yaIlluminant ill = ei_undefined,
      yaRGBPrimary prim = ergbp_undefined,
      yaColorSpaceMinor m = ecdm_undefined)
    : cs_major(mm),
      cs_minor(m),
      illuminant(ill),
      primary(prim)
    {}
  };
  

  // Predefined color spaces
  const static color_space 
    //! sRGB colorspace
    cs_sRGB(color_space::ecd_rgb),
    //! YCbCrRec601 colorspace
    cs_YCbCr601(color_space::ecd_ycbcr, color_space::ei_undefined, color_space::ergbp_undefined, color_space::ecdm_ycbcr_rec601),
    //! CIE RGB colorspace
    cs_CIERGB(color_space::ecd_rgb, color_space::ei_d65, color_space::ergbp_CIE),
    //! HLS with l1 norm colorspace
    cs_HLSl1(color_space::ecd_hls, color_space::ei_undefined, color_space::ergbp_undefined, color_space::ecdm_hls_l1),
    //! CIE LA*B* colorspace
    cs_CIELAB(color_space::ecd_lab, color_space::ei_d50, color_space::ergbp_CIE),
    //! ITU LA*B* colorspace
    cs_ITULAB(color_space::ecd_lab, color_space::ei_undefined, color_space::ergbp_CIE, color_space::ecdm_lab_itu),
    //! CMYK
    cs_CMYK(color_space::ecd_cmyk)
  ;
  
  
  /*!@brief Returns the radiation of the black-body at the specified wavelenght and temperature.
   * The wavelenght is in nanometer and the temperature is in Kelvin.
   * 
   * @param[in] wavelenght_nanometer The wavelenght, in nanometer
   * @param[in] temperature temperature, in Kelvin
   *  
   * @return the radiation of the black-body at the specified wavelenght and temperature. The returned value
   * may be huge. Consider the blackbody radiation normalized by a temperature.
   * @author Raffi Enficiaud   
   */
  YCom_ yaF_double blackbody_radiation(yaUINT32 const wavelenght_nanometer, yaUINT32 temperature);


  /*!@brief Returns the radiation of the black-body at the specified wavelenght and temperature, normalized by
   * the radiation at a specific temperature.
   
   * The wavelenght is in nanometer and the temperature is in Kelvin.
   * 
   * @param[in] wavelenght_nanometer The wavelenght, in nanometer
   * @param[in] temperature temperature, in Kelvin
   * @param[in] temperature_normalized temperature used for normalization, in Kelvin
   *  
   * @return the radiation of the black-body at the specified wavelenght and temperature, normalized by radiation of the black body at a
   * specific temperature.
   * @author Raffi Enficiaud   
   */
  YCom_ yaF_double blackbody_radiation_normalized(yaUINT32 const wavelenght_nanometer, yaUINT32 temperature, yaUINT32 temperature_normalized);


  


  //! @} // defgroup common_color_grp

}
 
 
#endif /* YAYI_COMMON_COLORSPACE_HPP__ */
