#ifndef YAYI_COMMON_CONFIG__HPP__
#define YAYI_COMMON_CONFIG__HPP__


/*!@file
 * Common configuration definitions. This file should not depend on any other file.
 */

#include <boost/config.hpp>

namespace yayi
{

#if defined(_WIN32) || defined(_WIN64)
  #define yayiDeprecated __declspec(deprecated)
  #define yayiDeprecatedM(x) __declspec(deprecated(x))
  
  #if defined(_M_X64) || defined(_WIN64)
    #define YAYI_64BITS
  #endif
  
#else
	#define yayiDeprecated __attribute__((deprecated))
  #define yayiDeprecatedM(x) yayiDeprecated

  #if defined(__LP64__)
    #define YAYI_64BITS
  #endif

#endif


/*! Should be defined if the modules are intended for dynamic linking
 */
#ifdef YAYI_DYNAMIC
#		define MODULE_IMPORT BOOST_SYMBOL_IMPORT
#		define MODULE_EXPORT BOOST_SYMBOL_EXPORT
#else
#		define MODULE_IMPORT
#		define MODULE_EXPORT
#endif

// plateform specific section
#if defined(_WIN32) || defined (_WIN64)
	
#	ifndef _CPPRTTI
#		error You should activate the RTTI for yayi to run properly
#	endif

#	ifndef _CPPUNWIND
#		error You should activate the exception handling for yayi to run properly
#	endif

#endif


// Compiler deficiencies

// Workaround for noexcept
#ifdef BOOST_NO_CXX11_NOEXCEPT
  #define noexcept throw()
#endif


//!@def YAYI_COMPILER_ERROR_FOR_UNSUPPORTED_TYPES__
//! Define this macro to raise a compilation error when trying to instanciate Image classes (or pixels) with types that are unsupported by the variants
#define YAYI_COMPILER_ERROR_FOR_UNSUPPORTED_TYPES__


}


#endif


