#ifndef YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_T_HPP__
#define YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_T_HPP__

//!@todo clean this file

#include <yayiLowLevelMorphology/highlevel_alternate.hpp>
#include <yayiPixelProcessing/include/image_arithmetics_t.hpp>
#include <yayiPixelProcessing/include/image_compare_T.hpp>
#include <yayiReconstruction/include/morphological_reconstruction_t.hpp>
#include <yayiCommon/common_errors.hpp>
#include <yayiPixelProcessing/include/image_copy_T.hpp>


namespace yayi
{
  namespace llmm
  {
    /*!
     * @addtogroup llm_details_grp
     *
     *@{
     */      

     //! @} doxygroup: llm_details_grp

  }
}

#endif /* YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_T_HPP__ */
