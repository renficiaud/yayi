#ifndef YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_HPP__
#define YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_HPP__


/*!@file
 * This file defines some common morphological alternate filters
 * @author Thomas Retornaz
 * @todo clean this file.
 */


#include <yayiLowLevelMorphology/yayiLowLevelMorphology.hpp>
#include <yayiImageCore/include/yayiImageCore.hpp>
#include <yayiStructuringElement/yayiStructuringElement.hpp>

namespace yayi {
  namespace llmm {
    /*!
     * @addtogroup llm_grp
     *
     *@{
     */   

    using namespace yayi::se;

    //! @} doxygroup: llm_grp
  }
}


#endif /* YAYI_LOWLEVEL_MORPHOLOGY_ALTERNATE_HPP__ */
