#ifndef SEGMENTATION_PYTHON_HPP__
#define SEGMENTATION_PYTHON_HPP__

#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/def.hpp>

namespace bpy = boost::python;

#include <yayiCommon/common_types.hpp>
#include <yayiImageCore/include/yayiImageCore.hpp>
#include <yayiSegmentation/yayiSegmentation.hpp>

#endif
