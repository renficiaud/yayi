#ifndef YAYI_COMMON_PYTHON_HPP__
#define YAYI_COMMON_PYTHON_HPP__


#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/def.hpp>

namespace bpy = boost::python;

#include <yayiCommon/common_types.hpp>

#endif /* YAYI_COMMON_PYTHON_HPP__ */
