#include <yayiCommonPython/common_python.hpp>
#include <yayiCommon/common_colorspace.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/enum.hpp>


void declare_colorspace() {
  
  using namespace yayi;

  
  bpy::enum_<yayi::color_space::yaColorSpaceMajor>("colorspace_major")
  .value("cs_undefined",   yayi::color_space::ecd_undefined)
  .value("cs_rgb",   yayi::color_space::ecd_rgb)
  .value("cs_hls",   yayi::color_space::ecd_hls)
  .value("cs_xyz",   yayi::color_space::ecd_xyz)
  .value("cs_xyY",   yayi::color_space::ecd_xyY)
  .value("cs_yuv",   yayi::color_space::ecd_yuv)
  .export_values()
  ;
  
  
  bpy::enum_<yayi::color_space::yaColorSpaceMinor>("colorspace_minor")
  .value("csm_undefined",   yayi::color_space::ecdm_undefined)
  .value("csm_hls_l1",      yayi::color_space::ecdm_hls_l1)
  .value("csm_hls_trig",    yayi::color_space::ecdm_hls_trig)
  .export_values()
  ;
  
  bpy::enum_<yayi::color_space::yaIlluminant>("illuminant")
  .value("ill_undefined",   yayi::color_space::ei_undefined)
  .value("ill_d50",         yayi::color_space::ei_d50)
  .value("ill_d55",         yayi::color_space::ei_d55)
  .value("ill_d65",         yayi::color_space::ei_d65)
  .value("ill_d75",         yayi::color_space::ei_d75)
  .value("ill_A",           yayi::color_space::ei_A)
  .value("ill_B",           yayi::color_space::ei_B)
  .value("ill_C",           yayi::color_space::ei_C)
  .value("ill_E",           yayi::color_space::ei_E)
  .value("ill_blackbody",   yayi::color_space::ei_blackbody)
  .export_values()
  ;

  bpy::enum_<yayi::color_space::yaRGBPrimary>("primaries")
  .value("prim_undefined",   yayi::color_space::ergbp_undefined)
  .value("prim_CIE",         yayi::color_space::ergbp_CIE)
  .value("prim_sRGB",        yayi::color_space::ergbp_sRGB)
  .value("prim_AdobeRGB",    yayi::color_space::ergbp_AdobeRGB)
  .value("prim_AppleRGB",    yayi::color_space::ergbp_AppleRGB)
  .value("prim_NTSCRGB",     yayi::color_space::ergbp_NTSCRGB)
  .value("prim_SecamRGB",    yayi::color_space::ergbp_SecamRGB)
  .export_values()
  ;
  
    
    
  bpy::class_<yayi::color_space>("colorspace", bpy::init<>())
  .def(bpy::init<color_space::yaColorSpaceMajor,bpy::optional<color_space::yaIlluminant, color_space::yaRGBPrimary, color_space::yaColorSpaceMinor> >())
  .def_readwrite("major",       &yayi::color_space::cs_major)
  .def_readwrite("minor",       &yayi::color_space::cs_minor)
  .def_readwrite("illuminant",  &yayi::color_space::illuminant)
  .def_readwrite("primary",     &yayi::color_space::primary)
  .def(bpy::self == bpy::self)                                      // __eq__
  .def("__str__",               &yayi::color_space::operator string_type)//&yayi::type::operator string_type)
  ;
 
  bpy::def(
    "blackbody_radiation", 
    yayi::blackbody_radiation, 
    "Returns the value of the black body radiator at the specified wavelength and temperature");
  bpy::def(
    "blackbody_radiation_normalized", 
    yayi::blackbody_radiation_normalized, 
    "Returns the value of the black body radiator at the specified wavelength and temperature, normalized"
    "by the black body radiation value at the same wavelength and specified normalization temperature");
  
}
